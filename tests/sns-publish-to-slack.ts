import { snsPublishToSlack } from "../src/lambdas/snsPublishToSlack"
import { Context, SNSEvent } from "aws-lambda";
// import { SnsTopics } from "employsure-core";

const event: SNSEvent = {
    "Records": [
        {
            "EventSource": "aws:sns",
            "EventVersion": "1.0",
            "EventSubscriptionArn": "arn:aws:sns:ap-southeast-2:711143483997:spinify-prod-syncAppData-dlq:f6818cb3-aa58-41ac-8468-185bc1c93577",
            "Sns": {
                "Type": "Notification",
                "MessageId": "799f39eb-dd60-591c-ad34-6160a63affda",
                "TopicArn": "arn:aws:sns:ap-southeast-2:711143483997:spinify-prod-syncAppData-dlq",
                "Subject": "subj",
                "Message": "{\n  \"country\": \"Australia\"\n}\n",
                "Timestamp": "2021-01-27T14:30:50.668Z",
                "SignatureVersion": "1",
                "Signature": "ODZTxHMt4nMtcEXsAdTOTDJGdmnpfxWa/zl5Deht28WQs22lu7yJmSwbqiNp1t+W2qVgOqeMNs0tRiSKiHLlwgdo21QqM+CHjV9aWykEJ8dWj8bt1N4kKLMO/V8hkHUIWPTmUeCYzwOy5Ui9nX3wZhoLOI2SbcKfETaCVUKEIxlds+L/3MLikSbSWFlikIe/OXFS0WBrjTwz7L4LzoDDcsKnZoGmtcPffQ6jw6ZG5/IUlrdw9yx8Ljv3foTcNq0PR/sshQnY9PmNl0hpO4hvfMMWhxITgAWb5nmf0lXWUra4NMcW9CFQ/+jRQRdLYAIVgtltEYziwMwyw2B+IimJkA==",
                "SigningCertUrl": "https://sns.ap-southeast-2.amazonaws.com/SimpleNotificationService-010a507c1833636cd94bdb98bd93083a.pem",
                "UnsubscribeUrl": "https://sns.ap-southeast-2.amazonaws.com/?Action=Unsubscribe&SubscriptionArn=arn:aws:sns:ap-southeast-2:711143483997:spinify-prod-syncAppData-dlq:f6818cb3-aa58-41ac-8468-185bc1c93577",
                "MessageAttributes": {
                    "RequestID": {
                        "Type": "String",
                        "Value": "261aac81-6551-4f61-9aa7-15c88b2c0ce3"
                    },
                    "ErrorCode": {
                        "Type": "String",
                        "Value": "200"
                    },
                    "ErrorMessage": {
                        "Type": "String",
                        "Value": "2021-01-27T14:30:50.501Z 261aac81-6551-4f61-9aa7-15c88b2c0ce3 Task timed out after 1.00 seconds"
                    }
                }
            }
        }
    ]
}
// const event: SNSEvent = {
//     Records: [{
//         EventVersion: "",
//         EventSubscriptionArn: "",
//         EventSource: "aws:sns",
//         Sns: {
//             SignatureVersion: "",
//             Timestamp: "",
//             Signature: "",
//             SigningCertUrl: "",
//             MessageId: "",
//             Message: JSON.stringify({
//                 "Errors": [{ "title": 'test title', "message": 'test message' }],
//                 "other stuff": [
//                     {
//                         "could": "be anything",
//                         "hello": "world"
//                     }
//                 ]
//             }),
//             MessageAttributes: {},
//             Type: "",
//             UnsubscribeUrl: "",
//             TopicArn: SnsTopics.EngErrorsDev,
//             Subject: "Fake Message Subject"
//         }
//     }
//     ]
// }

snsPublishToSlack(event, {} as Context, (error, success) => {
    if (error) {
        console.log(`callback() run with an error: ${error}.`)
        throw error;
    }
});