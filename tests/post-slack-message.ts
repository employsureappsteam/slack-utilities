import { getSlackAppConfig } from "../src/aws/ssm";
import { PostMessageArguments } from "../src/models/messages";
import { ChannelIDs, AppConfigPaths, LambdaNamesMap } from "employsure-core";
import { invokeLambda } from "../src/aws/lambda"

async function test() {
    const config = await getSlackAppConfig(AppConfigPaths.Development);

    const message = new PostMessageArguments(
        `Fresh message`,
        ChannelIDs.Engineering,
        config.OAuthToken
    )

    return await invokeLambda(LambdaNamesMap.postSlackMessage, message);
}
const result = test();
console.log(result);