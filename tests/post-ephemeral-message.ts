import { postEphemeralMessage } from "../src/libs/slack";
import { getSlackAppConfig } from "../src/aws/ssm";
import { PostMessageArguments } from "../src/models/messages";
import { ChannelIDs, AppConfigPaths } from "employsure-core";

async function test() {
    const config = await getSlackAppConfig(AppConfigPaths.Development);

    await postEphemeralMessage("UCEK71ESK", new PostMessageArguments("hello", ChannelIDs.Engineering, config.OAuthToken));
}

test()