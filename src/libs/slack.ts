import { createHmac } from "crypto";
import { Lambda } from "aws-sdk";
// import { LambdaNames, AWSConfig } from "../constants/aws";
import { SlackMessageTypes } from "../constants/slack";
import { PostMessageArguments, UpdateMessageArguments } from "../models/messages";
import { PostMessageResponsePayload, UpdateMessageResponsePayload } from "../models/slackPayloads";
import { getSlackAppConfig } from "../aws/ssm";
import { DefaultAttachment } from "../models/attachments";
import { ChatPostEphemeralArguments, WebClient } from "@slack/client";
import { ChannelIDs, LambdaNamesMap, AWS_REGION_DEFAULT, AWS_LAMBDA_API_VERSION, AppConfigPaths } from "employsure-core";


/**
 * Get colour for appropriate message type
 * 
 * @param attachment MessageAttachment Object to be modified
 * @param color 6 Digit hex code of color
 */
export function lookupMessageTypeColor(type: number): string {
    switch (type) {
        case SlackMessageTypes.Success: return "#4dcc0d";
        case SlackMessageTypes.Warning: return "#c2f013";
        case SlackMessageTypes.Failed: return "#d60a0a";
        case SlackMessageTypes.Info: return "#2B387F";
        default: return "#000000";
    }
}

/**
 * Parse URI from slack message into object
 * 
 * @param uri URI in the format "fieldA=textA&fieldB=textB"
 */
export function parseSlashPayload(uri: string) {
    return decodeURIComponent(uri)
        .split("&")
        .reduce((accum: any, val: string) => {
            let kvPair = val.split("=");
            accum[kvPair[0]] = kvPair[1];
            return accum;
        }, {});
}

/**
 * Verify the signature of a request received from slack
 * 
 * @param request 
 * @param path path of secret in SSM
 */
export function VerifySlackSignature(request: any, secret: string): boolean {
    // Time stamp passed by slack does not have milliseconds.
    const timeStamp = request.headers["X-Slack-Request-Timestamp"];
    const slackSignature = request.headers["X-Slack-Signature"];

    const sigBaseString = `v0:${timeStamp}:${request.body}`;
    const hmac = createHmac("sha256", secret)
        .update(sigBaseString);

    const computedSignature = "v0=" + hmac.digest("hex");

    return computedSignature === slackSignature;
}

/** 
 * Invoke lambda function to post slack message.
 *  
 * @param slackMessage 
 */
export function postSlackMessage(slackMessage: PostMessageArguments): Promise<PostMessageResponsePayload> {
    // console.log(`Preparing to invoke postSlackMessage: ${JSON.stringify(slackMessage)}`);

    let params: Lambda.Types.InvocationRequest = {
        FunctionName: LambdaNamesMap.postSlackMessage,
        Payload: JSON.stringify(slackMessage)
    };

    const lambda: Lambda = new Lambda({
        region: AWS_REGION_DEFAULT,
        apiVersion: AWS_LAMBDA_API_VERSION
    });

    return lambda.invoke(params).promise()
        .then(response => {
            if (response.StatusCode === 200) {
                if (typeof (response.Payload) === "string") {
                    return JSON.parse(response.Payload);
                } else {
                    return Promise.reject(`Response Payload is not a string ${response}`);
                }
            } else {
                return Promise.reject(`Response statuscode is not 200.`)
            }
        })
        .catch(error => {
            console.log(error);
            return error
        });
}

export async function postEphemeralMessage(user_id: string, slackMessage: PostMessageArguments) {
    const e_message: ChatPostEphemeralArguments = {
        channel: slackMessage.channel,
        text: slackMessage.text,
        user: user_id,
        attachments: slackMessage.attachments,
        link_names: true,
        parse: 'none'
    };

    return new WebClient(slackMessage.token).chat.postEphemeral(e_message)
        .then((data) => {
            console.log(`Message sent: ${JSON.stringify(data, null, 4)}`)
            return data;
        })
        .catch((error) => {
            console.error(error);
            return error;
        });

}

/**
 * Update Message
 * 
 * @param arg Arguments for updating a particular message.
 */
export function updateSlackMessage(arg: UpdateMessageArguments): Promise<UpdateMessageResponsePayload> {
    // console.log(`Preparing to invoke updateSlackMessage: ${JSON.stringify(arg)}`);

    let params: Lambda.Types.InvocationRequest = {
        FunctionName: LambdaNamesMap.updateSlackMessage,
        Payload: JSON.stringify(arg)
    };

    const lambda: Lambda = new Lambda({
        region: AWS_REGION_DEFAULT,
        apiVersion: AWS_LAMBDA_API_VERSION
    });

    return lambda.invoke(params).promise()
        .then(response => {
            if (response.StatusCode === 200) {
                if (typeof (response.Payload) === "string") {
                    return JSON.parse(response.Payload);
                } else {
                    return Promise.reject(`Response Payload is not a string ${response}`);
                }
            } else {
                return Promise.reject(`Response statuscode is not 200.`)
            }
        })
        .catch(error => {
            console.log(error);
            return error
        });
}

export async function postMessageToDevelopment(attachment: DefaultAttachment, type: 'error' | 'log' = 'log') {
    const config = await getSlackAppConfig(AppConfigPaths.Development);
    const message = new PostMessageArguments("", ChannelIDs.EngAlertsDev, config.OAuthToken);

    if (type === 'error') {
        attachment.color = 'danger';
    }

    message.attach(attachment);
    return postSlackMessage(message);
}
