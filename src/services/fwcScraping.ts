import { PostMessageArguments, UpdateMessageArguments, CODE_BLOCK } from "../models/messages";
import { postSlackMessage, postMessageToDevelopment, updateSlackMessage } from "../libs/slack";
import { ISlashCommandService, ActionService } from "../models/service"
import { ChannelIDs, LambdaNamesMap, AppConfigPaths } from "employsure-core";
import { invokeLambda } from "../aws/lambda";
import { ActionNames, PrimaryButton, DefaultButton } from "../models/actions";
import { DefaultAttachment, CallbackIDs } from "../models/attachments";

type RunButtonValue = {
    PermittedUserId: string
}

export const requestStartFWCScrape: ISlashCommandService = {
    ConfigPath: AppConfigPaths.Development,
    Execute: async (payload, config) => {
        if (payload.channel_id === ChannelIDs.Engineering) {
            const message = new PostMessageArguments(
                '',
                ChannelIDs.Engineering,
                config.OAuthToken
            );

            const attachment = new DefaultAttachment(
                'FWC Scrape',
                `Please confirm that you want to run FWC Scrape.`,
                CallbackIDs.FWCScrapeConfirmation
            );

            const runButtonValue: RunButtonValue = { PermittedUserId: payload.user_id };
            const value = JSON.stringify(runButtonValue);
            attachment.addAction(new PrimaryButton(ActionNames.Run, 'Run', value));
            attachment.addAction(new DefaultButton(ActionNames.Cancel, 'Cancel', value));
            message.attach(attachment)

            postSlackMessage(message);
            return { success: true, message: `${payload.user_name} has requested FWC scrape.` }
        } else {
            return { success: false, message: `Command must be run from the Engineering channel. (from <#${ChannelIDs.Engineering}>)` }
        }
    }
}

/**
 * Handle an 
 */
export const confirmStartFWCScrape: ActionService = {
    ConfigPath: AppConfigPaths.Development,

    Execute: async (payload, config) => {
        const actionName = payload.actions[0].name;
        const value = JSON.parse(payload.actions[0].value) as RunButtonValue;

        if (value.PermittedUserId !== payload.user.id) {
            return { success: false, message: `You are not permitted to use this action.` };
        }

        // Prepared message for updating
        const message = new UpdateMessageArguments(
            config.OAuthToken,
            payload.channel.id,
            '',
            payload.message_ts
        )

        // TODO: Fix this so message is only updated once the lambda invokes successfully...
        // Handle which button was pressed
        if (actionName === ActionNames.Run) {
            message.attach(new DefaultAttachment(
                "FWC Scrape",
                `:heavy_check_mark: FWC Scrape has started.`
            ));

            invokeLambda(LambdaNamesMap.FWCLeadScrapper, {})
                .then(data => {
                    postSlackMessage(new PostMessageArguments(
                        `<@${payload.user.id}> Started FWC Scrape.`,
                        ChannelIDs.EngAlertsDev,
                        config.OAuthToken
                    ));
                })
                .catch(error => {
                    postMessageToDevelopment(new DefaultAttachment(
                        'Failed to invoke FWC Scrape',
                        `<@${payload.user.id}> tried to start FWC Scrape. Failed:\n${CODE_BLOCK}${error}${CODE_BLOCK}`
                    ), 'error');
                    console.log(`Error:`, error);
                });
        } else if (actionName === ActionNames.Cancel) {
            message.attach(new DefaultAttachment(
                "FWC Scrape",
                `:negative_squared_cross_mark: FWC was not started.`
            ));
        }

        updateSlackMessage(message);
        return { success: true, message: "FWC confirmation handled." };
    }
}