import { PostMessageArguments, UpdateMessageArguments } from "../models/messages";
import { postSlackMessage, updateSlackMessage } from "../libs/slack";
import { ISlashCommandService, ActionService } from "../models/service"
import { DefaultAttachment, CallbackIDs } from "../models/attachments";
import { PrimaryButton, DefaultButton, ActionNames } from "../models/actions";
import { ChannelIDs, AppConfigPaths, LambdaNamesMap } from "employsure-core";
import { invokeLambda } from "../aws/lambda";

type NzLeadScrapeParams = {
    number_of_cases: number,
    keywords: string
}

type RunButtonValue = {
    PermittedUserId: string
}

export const requestNZLeadScrape: ISlashCommandService = {
    ConfigPath: AppConfigPaths.Development,
    Execute: async (payload, config) => {
        if (payload.channel_id === ChannelIDs.Engineering) {
            const message = new PostMessageArguments(
                '',
                ChannelIDs.Engineering,
                config.OAuthToken
            );

            const attachment = new DefaultAttachment(
                "NZ Leads Search",
                `Please confirm that you want to run NZ Lead Search.`,
                CallbackIDs.NZLeadScrapeConfirmation
            );

            const runButtonValue: RunButtonValue = { PermittedUserId: payload.user_id };
            const value = JSON.stringify(runButtonValue);
            attachment.addAction(new PrimaryButton(ActionNames.Run, "Run", value));
            attachment.addAction(new DefaultButton(ActionNames.Cancel, "Cancel", value));
            message.attach(attachment)

            postSlackMessage(message);
        } else {
            return { success: false, message: `<@${payload.user_id}> tried to start NZ Lead Scrape.\nFailed because the command must be run from the Development channel.` }
        }

        return { success: true, message: "Handled request for NZ Lead Search Successfully." };
    }
}

export const confirmNZLeadSearch: ActionService = {
    ConfigPath: AppConfigPaths.Development,

    Execute: async (payload, config) => {
        const actionName = payload.actions[0].name;
        const value = JSON.parse(payload.actions[0].value) as RunButtonValue;

        if (value.PermittedUserId !== payload.user.id) {
            return { success: false, message: `<@${payload.user.id}> is not permitted to use this action.` };
        }

        const message = new UpdateMessageArguments(
            config.OAuthToken,
            payload.channel.id,
            '',
            payload.message_ts
        )

        if (actionName === ActionNames.Run) {
            message.attach(new DefaultAttachment(
                "NZ Lead Search",
                `:heavy_check_mark: NZ Lead Search has started.`
            ));
            const nzLeadScrapeParams: NzLeadScrapeParams = {
                number_of_cases: 30,
                keywords: "Unjustified Dismissal"
            }

            invokeLambda(LambdaNamesMap.NZLeadSrapper, nzLeadScrapeParams)
                .then(data => console.log(`Succcess: fwcScrape invoke: ${JSON.stringify(data.Payload)}`))
                .catch(error => console.log(`Error: [${error.code}] ${error.message}`));
        } else if (actionName === ActionNames.Cancel) {
            message.attach(new DefaultAttachment(
                "NZ Lead Search",
                `:negative_squared_cross_mark: NZ Lead Search was cancelled.`
            ));
        }

        updateSlackMessage(message)
        return { success: true, message: "Handled Confirmation sucessfully" };
    }
}