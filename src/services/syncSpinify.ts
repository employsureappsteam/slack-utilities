import { ISlashCommandService } from "../models/service"
// import { invokeLambda } from "../aws/lambda";
import { AppConfigPaths } from "employsure-core";
import { invokeLambda } from "../aws/lambda";

export const syncSpinify: ISlashCommandService = {
    ConfigPath: AppConfigPaths.Provisioning,
    AllowedChannels: ['CNPDVTEH4'], // https://employsure-solutions.slack.com/archives/CNPDVTEH4
    Execute: async (payload, config) => {
        const args = payload.text.split("+");
        const country = args[0];
        const bearerToken = args[1];

        if (!['Australia', 'NewZealand'].includes(country)) {
            return {
                success: false,
                message: `Country '${country}' is invalid. Must be 'Australia' or 'NewZealand'`
            }
        }

        console.log(`syncPingboard executed by ${payload.user_name} from ${payload.channel_id}.`);

        await invokeLambda(
            'arn:aws:lambda:ap-southeast-2:711143483997:function:spinify-prod-syncAppData',
            { country, bearerToken }
        );

        return {
            success: true,
            message: `Started Spinify sync for ${country}`
        }
    }
}
