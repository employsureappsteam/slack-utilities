import { PostMessageArguments, UpdateMessageArguments } from "../models/messages";
import { ISlashCommandService, ActionService } from "../models/service"
import { invokeLambda } from "../aws/lambda";
import { ChannelIDs, LambdaEvent, AppConfigPaths, LambdaNamesMap } from "employsure-core";
import { ActionNames, PrimaryButton, DefaultButton } from "../models/actions";
import { DefaultAttachment, CallbackIDs } from "../models/attachments";
import { updateSlackMessage } from "../libs/slack";

export const invokeWorkplaceProvisioning: ISlashCommandService = {
    ConfigPath: AppConfigPaths.Provisioning,

    Execute: async (payload, config) => {
        const arg = payload.text;

        /* If not from provisioning or engineering. */
        if (payload.channel_id !== ChannelIDs.Provisioning && payload.channel_id !== ChannelIDs.EngAlertsDev) {
            /* Can be secretly run from #eng-alerts-dev */
            return { success: false, message: `${payload.command} can only be run from #provisioning.` }
        } else if (payload.channel_id === ChannelIDs.EngAlertsDev && arg !== "Dev" && arg !== "Prod") {
            return { success: false, message: `Failed to run ${payload.command}. Incorrect araguments. Format: \n\`${payload.command} ["Dev" | "Prod"]\`` }
        } else {
            const stage = payload.channel_id === ChannelIDs.Provisioning ? "Prod" : arg;
            const title = payload.channel_id === ChannelIDs.Provisioning ? "Workplace Provisioning" : `Workplace Provisioning [${arg}]`

            const message = new PostMessageArguments('', payload.channel_id, config.OAuthToken)
            const attachment = new DefaultAttachment(title, `<@${payload.user_name}> please choose one of the following actions.`, CallbackIDs.WorkplaceProvisioningAction);
            const runButtonValue: ButtonValue = { PermittedUserId: payload.user_id, Stage: stage };
            const buttonValue = JSON.stringify(runButtonValue);

            attachment.addAction(new PrimaryButton(ActionNames.ProvisionWorkplace, 'Provision Workplace', buttonValue));
            if (payload.channel_id === ChannelIDs.EngAlertsDev) {
                // Also attach button to update admins if run from secret eng channel
                attachment.addAction(new PrimaryButton(ActionNames.UpdateWorkplaceAdmins, 'Update Workplace Admins', buttonValue));
            }
            attachment.addAction(new DefaultButton(ActionNames.Cancel, 'Cancel', buttonValue));
            message.attach(attachment)

            await invokeLambda(LambdaNamesMap.postSlackMessage, message)

            console.log(`Sent confirmation to user ${payload.user_name} in channel ${payload.channel_name} (${payload.channel_id}) for stage ${arg}.`);
            return { success: true }
        }
    }
}

type ButtonValue = {
    PermittedUserId: string,
    Stage: string // "Prod" | "Dev"
}

export const confirmWorkplaceProvisioning: ActionService = {
    ConfigPath: AppConfigPaths.Provisioning,

    Execute: async (payload, config) => {
        const actionName = payload.actions[0].name;
        const { PermittedUserId, Stage } = JSON.parse(payload.actions[0].value) as ButtonValue;

        if (PermittedUserId !== payload.user.id) {
            return { success: false, message: `You don't have permission to use this.` };
        }

        if (Stage !== "Dev" && Stage !== "Prod") {
            return { success: false, message: `Invalid stage ${Stage}.` };
        }

        const lambdaName = `provisioning-${Stage.toLowerCase()}-provisionWorkplace`
        const title = Stage === "Dev" ? "Dev | Workplace Provisioning" : "Workplace Provisioning";

        /* Make blank message to attach to depending on button pressed. */
        const message = new UpdateMessageArguments(
            config.OAuthToken,
            payload.channel.id,
            '',
            payload.message_ts
        )

        /* Handle each button. */
        const eventPayload = {
            Author: payload.user.name,
            Stage,
            Source: 'emp.slack',
            options: {
                provisionUsers: true,
                updateUsers: true,
                updateGroupMembers: true,
                updateGroupAdmins: false,
            }
        }

        let text = ''
        let doInvoke = false
        if (actionName === ActionNames.ProvisionWorkplace) {
            doInvoke = true;
            text = `:heavy_check_mark: Workplace provisioning started by <@${payload.user.name}>.`;
        } else if (actionName === ActionNames.UpdateWorkplaceAdmins) {
            eventPayload.options.provisionUsers = false;
            eventPayload.options.updateUsers = false;
            eventPayload.options.updateGroupAdmins = false;
            eventPayload.options.updateGroupAdmins = true;
            doInvoke = true;
            text = `:heavy_check_mark: Workplace group admin update started by <@${payload.user.name}>.`
        } else if (actionName === ActionNames.Cancel) {
            text = `:negative_squared_cross_mark: Workplace provisioning cancelled by <@${payload.user.name}>.`;
        }

        if (doInvoke) {
            await invokeLambda(lambdaName, eventPayload);
        }
        message.attach(new DefaultAttachment(title, text));
        updateSlackMessage(message);
        return { success: true, message: `Workplace Provision Handled. action: ${actionName}, stage: ${Stage}` };
    }
}

/**
 * To do with retry events. The message can be posted from anywhere. 
 * To trigger this action the action must have the callback id as mapped in actionHandler.ts.
 * In this case, CallbackIDs.WorkplaceProvisionRetry
 */
export const retryProvisionWorkplace: ActionService = {
    ConfigPath: AppConfigPaths.Provisioning,
    Execute: async (payload, config) => {
        const actionName = payload.actions[0].name;
        const { Stage, RetryCount, options } = JSON.parse(payload.actions[0].value);

        /* Make blank message to attach to depending on button pressed*/
        const message = new UpdateMessageArguments(
            config.OAuthToken,
            payload.channel.id,
            '',
            payload.message_ts
        )

        if (Stage === undefined ||
            RetryCount === undefined ||
            options === undefined) {
            updateSlackMessage(new UpdateMessageArguments(config.OAuthToken, payload.channel.id, `Failed to retry workplace provisioning`, payload.message_ts));
            return { success: false, message: `Can't retry workplace. Missing button parameters.` }
        }

        const lambda = Stage === "Prod" ? LambdaNamesMap.ProvisionWorkplaceProd : LambdaNamesMap.ProvisionWorkplaceDev;
        const titlePrefix = Stage === "Prod" ? "" : "Dev | ";

        const eventPayload: LambdaEvent<any> = {
            Source: 'emp.slack',
            Author: `${payload.user.name}`,
            RetryCount: RetryCount + 1,
            Stage,
            options
        }

        const title = `${titlePrefix}Workplace Provisioning finished with errors`;

        if (actionName === ActionNames.Cancel) {
            message.attach(new DefaultAttachment(title, `:negative_squared_cross_mark: Workplace provisioning retry cancelled.`));
        } else if (actionName === ActionNames.ProvisionWorkplace) {
            invokeLambda(lambda, eventPayload);
            message.attach(new DefaultAttachment(title, `:heavy_check_mark: Retrying Workplace provisioning.`));
        } else {
            message.attach(new DefaultAttachment(title, `:negative_squared_cross_mark: Something went wrong. :negative_squared_cross_mark:`));
        }

        updateSlackMessage(message);

        return { success: true, message: `Workplace Provision Handled. action: ${actionName}, stage: ${Stage}` };
    }
}