import { ISlashCommandService } from "../models/service"
import { invokeLambda } from "../aws/lambda";
import { ChannelIDs, AppConfigPaths } from "employsure-core";

export const syncPingboard: ISlashCommandService = {
    ConfigPath: AppConfigPaths.Provisioning,

    Execute: async (payload, config) => {
        /* Must be run from Provisioning channel */
        if (payload.channel_id !== ChannelIDs.Provisioning) {
            return { success: false, message: `${payload.command} can only be run from #provisioning.` }
        } else {
            console.log(`syncPingboard executed by ${payload.user_name} from ${payload.channel_id}.`);
            await invokeLambda('arn:aws:lambda:ap-southeast-2:711143483997:function:pingboard-prod-sync', {});

            return {
                success: true,
                message: `Pingboard sync started.`
            }
        }
    }
}
