import { StepFunctions, AWSError } from "aws-sdk";
import { postSlackMessage } from "../libs/slack";

import { FailedMessageAttachement, CallbackIDs } from "../models/attachments";
import { PostMessageArguments } from "../models/messages";
import { ActionNames } from "../models/actions";
import { SlashCommandPayload, ActionPayload } from "../models/slackPayloads";
import { ActionService, ISlashCommandService, SlackConfig } from "../models/service";
import { ChannelIDs, AppConfigPaths, AWS_REGION_DEFAULT, AWS_STEP_FUNCTIONS_API_VERSION, SfArnMap } from "employsure-core";

export const pureCloudFailedResponse: ActionService = {
    ConfigPath: AppConfigPaths.AWSNotificationService,
    Execute: actionPureCloudFailedResponse
}

export const startPureCloudSync: ISlashCommandService = {
    ConfigPath: AppConfigPaths.AWSNotificationService,
    Execute: slashStartPureCloudSync
}

async function slashStartPureCloudSync(payload: SlashCommandPayload, config: SlackConfig) {
    const method = payload.text
    const user = payload.user_name
    const origin = "Slack Slash Command"

    // Setup pure cloud step function
    const stepfunctions = new StepFunctions({
        region: AWS_REGION_DEFAULT,
        apiVersion: AWS_STEP_FUNCTIONS_API_VERSION
    });

    // Parameters for starting step function
    const stepFunctionParams: StepFunctions.Types.StartExecutionInput = {
        stateMachineArn: SfArnMap.syncPureCloudData,
        input: JSON.stringify({
            pureCloudExecution: {
                method: method
            }
        }),
        name: `${user}-Retry_${method}-${Date.now()}`,
    };

    // Initiate pure cloud sync step function and post result to slack
    stepfunctions.startExecution(stepFunctionParams, (error: AWSError, data: StepFunctions.Types.StartExecutionOutput) => {
        let slackMessage = new PostMessageArguments(
            `[${origin}] ${method} triggered by ${user}`,
            ChannelIDs.PureCloudSync,
            config.OAuthToken
        );

        if (error) {
            slackMessage.attach(new FailedMessageAttachement(
                `Failed to execute. Error: ${error.message}`,
                CallbackIDs.FailedSync,
                "retry"
            ));
        }

        postSlackMessage(slackMessage);
    });

    return { success: true, message: "Pure cloud sync started" };
}


/**
 * Handle fail attachment for pure cloud sync failing.
 * 
 * Will be either acknolwedge or retry.
 * 
 * @param user Name of user invoking
 * @param method Method name to pass to sync
 * @param origin Where you are calling this from (e.g. slash command, retry button)
 */
async function actionPureCloudFailedResponse(payload: ActionPayload, config: SlackConfig) {
    const origin = "Fail attachment";
    const user = payload.user.name;
    const action = payload.actions[0].name;
    const messageValue = payload.actions[0].value;
    const method = messageValue.pureCloudExecution.method;

    if (action === ActionNames.Acknowledge) {
        postSlackMessage(new PostMessageArguments(
            `${user} has acknowledge error. Method: ${method}.`,
            ChannelIDs.PureCloudSync,
            config.OAuthToken
        ));
    } else if (action === ActionNames.Retry) {
        // Setup pure cloud step function
        const stepfunctions = new StepFunctions({
            region: AWS_REGION_DEFAULT,
            apiVersion: AWS_STEP_FUNCTIONS_API_VERSION
        });

        // Parameters for starting step function
        const stepFunctionParams: StepFunctions.Types.StartExecutionInput = {
            stateMachineArn: SfArnMap.syncPureCloudData,
            input: JSON.stringify({
                pureCloudExecution: {
                    method: method
                }
            }),
            name: `${user}-Retry_${method}-${Date.now()}`,
        };

        // Initiate pure cloud sync step function and post result to slack
        stepfunctions.startExecution(stepFunctionParams, (error: AWSError, data: StepFunctions.Types.StartExecutionOutput) => {
            let slackMessage = new PostMessageArguments(
                `[${origin}] ${method} triggered by ${user}`,
                ChannelIDs.PureCloudSync,
                config.OAuthToken
            );

            if (error) {

                slackMessage.attach(new FailedMessageAttachement(
                    `Failed to execute. Error: ${error.message}`,
                    CallbackIDs.FailedSync,
                    "retry"
                ));
            }

            postSlackMessage(slackMessage);
        });

        postSlackMessage(new PostMessageArguments(
            `${user} has retriggered a retry for ${method}.`,
            ChannelIDs.PureCloudSync,
            config.OAuthToken
        ));
    }

    return { success: true, message: "started" };
}
