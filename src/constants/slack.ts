export enum SlackMessageTypes {
  Info,
  Success,
  Warning,
  Failed,
  Default
}