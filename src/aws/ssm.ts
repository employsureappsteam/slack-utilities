import { SSM } from "aws-sdk";
import { SlackConfig } from "../models/service";
import { AWS_REGION_DEFAULT, AWS_SSM_API_VERSION } from "employsure-core";


// TODO: Move into employsure-config
/* Parameters mapped to Path -> Value .*/
export type ParametersMap = { [path: string]: string }
export interface ParametersResult {
    Parameters: ParametersMap,
    Invalid: string[];
};

const ssm = new SSM({
    region: AWS_REGION_DEFAULT,
    apiVersion: AWS_SSM_API_VERSION
});

const SlackAppConfigParams = {
    SigningSecret: "/SigningSecret",
    OAuthToken: "/OAuthToken"
}

/**
 * Get signing secret from SSM
 * @param token App Vertification Token
 */
export function getSlackAppConfig(rootPath: string): Promise<SlackConfig> {
    return new Promise((resolve, reject) => {
        // SSM get parameters
        const ssmParams = {
            Names: [
                rootPath + SlackAppConfigParams.SigningSecret,
                rootPath + SlackAppConfigParams.OAuthToken
            ],
            WithDecryption: true
        };

        // Get slack signing secret from SSM
        ssm.getParameters(ssmParams, (error, data) => {
            if (error) {
                reject(`AWSError [${error.code}]. ${error.message} `);
            } else {
                const results = data.Parameters;
                if (!results) {
                    reject(`Cannot find anything at ${rootPath}`);
                } else {
                    const signingSecret = results.find(p => p.Name === rootPath + SlackAppConfigParams.SigningSecret);
                    const OAuthToken = results.find(p => p.Name === rootPath + SlackAppConfigParams.OAuthToken);

                    if (!signingSecret) {
                        reject(`Cannot find signingSecret ${rootPath}`)
                    } else if (!OAuthToken) {
                        reject(`Cannot find OAuthToken ${rootPath}`)
                    } else {
                        const config: SlackConfig = {
                            SigningSecret: signingSecret.Value as string,
                            OAuthToken: OAuthToken.Value as string
                        }
                        resolve(config);
                    }
                }
            }
        });
    });
}

export function getParameter(path: string, WithDecryption: boolean = false): Promise<string | undefined> {
    return new Promise((resolve, reject) => {
        const ssmParams = {
            Name: path,
            WithDecryption
        };
        ssm.getParameter(ssmParams, (error, data) => {
            if (error) {
                reject(`AWSError [${error.code}]. ${error.message} `);
            } else {
                if (data.Parameter === undefined || data.Parameter.Value === undefined) {
                    resolve(undefined);
                } else {
                    resolve(data.Parameter.Value);
                }
            }
        });
    });
}


/**
 * Get signing secret from SSM
 * @param token App Vertification Token
 */
export function getParameters(paths: string[], WithDecryption: boolean = false): Promise<ParametersResult> {
    return new Promise((resolve, reject) => {
        const ssmParams = {
            Names: paths,
            WithDecryption
        };

        ssm.getParameters(ssmParams, (error, data) => {
            if (error) {
                reject(`AWSError [${error.code}]. ${error.message} `);
            } else {
                const Invalid = data.InvalidParameters ? data.InvalidParameters : [];
                const ParameterList = data.Parameters ? data.Parameters : [];
                const Parameters = ParameterList.reduce((obj: ParametersMap, param) => {
                    obj[param.Name as string] = param.Value as string;
                    return obj;
                }, {})
                resolve({ Invalid, Parameters });
            }
        });
    });
}

export function putParameter(path: string, Value: string, Type: 'String' | 'SecureString', Overwrite: boolean = false) {
    const ssmParams: SSM.PutParameterRequest = {
        Name: path,
        Value,
        Type,
        Overwrite
    }

    ssm.putParameter(ssmParams, (err, data) => {
        if (err) {
            console.log(err);
        } else {
            console.log(data);
        }
    });
}