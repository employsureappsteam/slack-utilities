import { Lambda } from "aws-sdk";
import { AWS_REGION_DEFAULT, AWS_LAMBDA_API_VERSION } from "employsure-core";

/**
 * 
 * @param functionName name of
 * @param eventPayload 
 */
export function invokeLambda<T>(functionName: string, eventPayload: T) {
    const lambda = new Lambda({
        region: AWS_REGION_DEFAULT,
        apiVersion: AWS_LAMBDA_API_VERSION
    });

    const invokeRequest: Lambda.Types.InvocationRequest = {
        FunctionName: functionName,
        Payload: JSON.stringify(eventPayload),
        InvocationType: 'Event'
    };

    return lambda.invoke(invokeRequest).promise();
}