import { VerifySlackSignature, postMessageToDevelopment, postEphemeralMessage } from "../libs/slack";
import { ActionPayload } from "../models/slackPayloads";
import { SlackEmptyResponse } from "../models/callbackResults";
import { Handler, Context, Callback } from "aws-lambda";
import { getSlackAppConfig } from "../aws/ssm";
import { ActionService } from "../models/service";

// Services
import { pureCloudFailedResponse } from "../services/pureCloudSync";
import { PostMessageArguments, CODE_BLOCK } from "../models/messages";
import { confirmNZLeadSearch } from "../services/nzLeadSearch";
import { CallbackIDs, DefaultAttachment } from "../models/attachments";
import { confirmStartFWCScrape } from "../services/fwcScraping";
import { confirmWorkplaceProvisioning, retryProvisionWorkplace } from "../services/provisionWorkplace";


// Add slash commands here, in the form of "<slash command name>: <function to be run>"
const Actions: { [name: string]: ActionService } = {
    [CallbackIDs.FailedSync]: pureCloudFailedResponse,
    [CallbackIDs.NZLeadScrapeConfirmation]: confirmNZLeadSearch,
    [CallbackIDs.FWCScrapeConfirmation]: confirmStartFWCScrape,
    [CallbackIDs.WorkplaceProvisioningAction]: confirmWorkplaceProvisioning,
    [CallbackIDs.WorkplaceProvisionRetry]: retryProvisionWorkplace
};

export const actionHandler: Handler = async (event: any, context: Context, callback: Callback) => {
    console.log("actionHandler triggered. Event: " + JSON.stringify(event, null, 4));

    // Decode URI and remove "payload=" at start of string
    const payloadString = decodeURIComponent(event.body).replace("payload=", "");
    const payloadJSON: ActionPayload = JSON.parse(payloadString);

    console.log(`payload: ${JSON.stringify(payloadJSON, null, 4)}`);

    handleActionPayload(event, payloadJSON);

    callback(null, new SlackEmptyResponse());
}

async function handleActionPayload(request: any, actionPayload: ActionPayload) {
    if (!Actions[actionPayload.callback_id]) {
        postMessageToDevelopment(
            new DefaultAttachment(
                `Slack Action ${actionPayload.callback_id} does not exist`,
                `Couldn't find an action for callback id \`${actionPayload.callback_id}\`, slack action payload is below:\n${CODE_BLOCK}${JSON.stringify(actionPayload, null, 4)}${CODE_BLOCK}`
            ),
            'error'
        );
        return;
    }

    const action = Actions[actionPayload.callback_id];
    const config = await getSlackAppConfig(action.ConfigPath);
    if (!VerifySlackSignature(request, config.SigningSecret)) {

        postMessageToDevelopment(
            new DefaultAttachment(
                'Cannot verify slack signature.',
                `${CODE_BLOCK}${JSON.stringify(actionPayload, null, 4)}${CODE_BLOCK}`
            ),
            'error'
        );
        return;
    }

    const result = await action.Execute(actionPayload, config);
    if (result.success === false) {
        await postEphemeralMessage(
            actionPayload.user.id,
            new PostMessageArguments(result.message, actionPayload.channel.id, config.OAuthToken)
        );
    } else {
        console.log(`Slash command finished: ${result.message}`);
    }
}