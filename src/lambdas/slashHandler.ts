// Types
import { SlashCommandPayload } from "../models/slackPayloads";
import { ISlashCommandService } from "../models/service";
import { SlackEmptyResponse } from "../models/callbackResults";
import { Handler } from "aws-lambda";

// Libs
import { parseSlashPayload, VerifySlackSignature, postEphemeralMessage, postMessageToDevelopment } from "../libs/slack";
import { getSlackAppConfig } from "../aws/ssm";

// Services
import { startPureCloudSync } from "../services/pureCloudSync";
import { requestStartFWCScrape } from "../services/fwcScraping";
import { requestNZLeadScrape } from "../services/nzLeadSearch";
import { invokeWorkplaceProvisioning } from "../services/provisionWorkplace";
import { PostMessageArguments, CODE_BLOCK } from "../models/messages";
import { DefaultAttachment } from "../models/attachments";
import { syncPingboard } from "../services/syncPingboard";
import { syncSpinify } from "../services/syncSpinify";

// Add slash commands here, in the form of "<slash command name>: <function to be run>"
const Commands: { [name: string]: ISlashCommandService } = {
    "no function yet": startPureCloudSync,
    "/fwcscrape": requestStartFWCScrape,
    "/nzleadscrape": requestNZLeadScrape,
    "/provisionworkplace": invokeWorkplaceProvisioning,
    "/syncpingboard": syncPingboard,
    "/syncspinify": syncSpinify
};

export const slashHandler: Handler = async (event: any) => {
    console.log(`slashHandler triggered. Event: ${JSON.stringify(event, null, 4)}`);

    const slashPayload: SlashCommandPayload = parseSlashPayload(event.body)
    console.log(`payload: ${JSON.stringify(slashPayload, null, 4)}`);

    await handleSlackPayload(event, slashPayload);

    return new SlackEmptyResponse();
}

async function handleSlackPayload(request: any, slashPayload: SlashCommandPayload) {
    const command = Commands[slashPayload.command];
    if (command === undefined) {
        await postMessageToDevelopment(new DefaultAttachment(`${slashPayload.command} does not exist.`, `${CODE_BLOCK}${JSON.stringify(slashPayload, null, 4)}${CODE_BLOCK}`), 'error');
        return;
    }

    const config = await getSlackAppConfig(command.ConfigPath);
    if (!VerifySlackSignature(request, config.SigningSecret)) {
        await postMessageToDevelopment(new DefaultAttachment('Cannot verify slack signature.', `${CODE_BLOCK}${JSON.stringify(slashPayload, null, 4)}${CODE_BLOCK}`), 'error');
        return;
    }

    /* Check command can be launched from this channel. */
    const allowedChannels = command.AllowedChannels;
    if (allowedChannels !== undefined && !allowedChannels.includes(slashPayload.channel_id)) {
        await postEphemeralMessage(
            slashPayload.user_id, new PostMessageArguments(
                `${slashPayload.command} cannot be run from this channel.`,
                slashPayload.channel_id,
                config.OAuthToken
            )
        )
        return;
    }

    try {
        const result = await command.Execute(slashPayload, config);
        if (result.message) {
            await postEphemeralMessage(slashPayload.user_id, new PostMessageArguments(result.message, slashPayload.channel_id, config.OAuthToken));
        }
    } catch (error) {
        console.log(error);
        await postEphemeralMessage(slashPayload.user_id, new PostMessageArguments(`Server Error. Please contact Engineering.`, slashPayload.channel_id, config.OAuthToken));
    }
}