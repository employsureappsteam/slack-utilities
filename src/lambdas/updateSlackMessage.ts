import { WebClient } from "@slack/client";
import { UpdateMessageArguments } from "../models/messages";

export async function updateSlackMessage(
    event: UpdateMessageArguments,
    context: any,
    callback: any
) {
    console.log(`updateSlackMessage. Event: ${JSON.stringify(event, null, 4)}`);

    // Create new webclient, update message, handle Promise
    const result = await new WebClient(event.token).chat.update(event)
        .then((data: any) => {
            console.log(`Message updated: ${JSON.stringify(data, null, 4)}`)
            return data;
        })
        .catch((error) => {
            console.error(error);
            return error;
        });

    callback(null, result);
}