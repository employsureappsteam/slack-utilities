import { Handler, SNSEvent, SQSEvent, SQSRecord, SNSEventRecord } from "aws-lambda";
import { getSlackAppConfig } from "../aws/ssm";
import { PostMessageArguments } from "../models/messages";
import { TopicAttachment } from "../models/attachments";
import { postSlackMessage } from "../libs/slack";
import { SnsMessage } from "../models/sns";
import { GetSlackChannelID, getDynamoTableRow } from "employsure-core";
import { DynamoDB } from "aws-sdk";

export const snsPublishToSlack: Handler = async (event: SNSEvent | SQSEvent) => {
    let resultMessage = undefined;
    try {
        if (!event.Records) {
            console.log(`event: ${JSON.stringify(event, null, 4)}`);
            throw new Error(`Event has no records.`);
        }
        const record = event.Records[0];

        const isFromSqs = (event.Records[0] as SQSRecord).eventSource === "aws:sqs";
        const isFromSns = (event.Records[0] as SNSEventRecord).EventSource === "aws:sns";

        if (isFromSqs) {
            resultMessage = await sendFromSqs(record as SQSRecord);
        } else if (isFromSns) {
            resultMessage = await sendFromSns(record as SNSEventRecord)
        } else {
            console.log(`event: ${JSON.stringify(event, null, 4)}`);
            throw new Error(`This lambda can only be invoked by SQS or SNS.`);
        }
    } catch (error) {
        resultMessage = `${error.name}: ${error.message}`;
    }
    console.log(resultMessage);
}

/** Assume this record is from a DLQ. */
async function sendFromSqs(record: SQSRecord) {
    const arn = record.eventSourceARN;
    const topicConfig = await GetSnsTopicSlackConfig(arn);
    const slackChannel = await GetSlackChannelID(topicConfig.SlackChannelName);
    if (!slackChannel.Id) {
        throw new Error(`SlackChannel missing Id`);
    }
    if (!topicConfig.Enabled) {
        return `Slack notifications for ${topicConfig.SnsTopicArn} are disabled.`;
    }
    const slackConfig = await getSlackAppConfig('/Slack/' + topicConfig.SlackAppName);

    const messageSenderId = record.attributes.SenderId;
    const subject = `AWS Dead Letter Received`;
    const title = `Failed to process message sent by ${messageSenderId}`;
    const text = `See logs for the lambda that received the original message to see why it failed.`;
    const attributeLog = Object.entries(record.attributes).map(([k, v]) => `${k}: ${v}`);
    attributeLog.push(`MessageId: ${record.messageId}`);
    const details = attributeLog.join(`\n`);

    const message = new PostMessageArguments(subject, slackChannel.Id, slackConfig.OAuthToken);
    const attachment = new TopicAttachment(title, text, 'danger', details);
    message.attach([attachment]);
    await postSlackMessage(message);
    return `Relayed SQS message from ${arn} to ${slackChannel.Name}`;
}

/* Handle a SNS record will well definied attributes. */
async function sendFromSns(record: SNSEventRecord) {
    const sns = record.Sns;
    const arn = sns.TopicArn;
    console.log(`Sns: ${JSON.stringify(sns, null, 4)}`);

    const topicConfig = await GetSnsTopicSlackConfig(arn);
    if (!topicConfig) {
        throw new Error(`Cannot find Sns Topic Config for ${arn}`);
    }

    const slackChannel = await GetSlackChannelID(topicConfig.SlackChannelName);
    if (!slackChannel || !slackChannel.Id) {
        throw new Error(`Cannot find Slack Channel ID for ${topicConfig.SlackChannelName}`);
    }

    if (topicConfig.Enabled === false) {
        return `${topicConfig.SnsTopicArn} slack notifications are disabled.`;
    }

    const slackConfig = await getSlackAppConfig('/Slack/' + topicConfig.SlackAppName);

    /* Make Slack Message. */
    let subject = sns.Subject;
    if (topicConfig.IsDeadLetterQueue) {
        const snsTopicName = arn.split(':')[5];
        subject = `DLQ Notification (from SNS ${snsTopicName})`;
    }
    const message = new PostMessageArguments(subject, slackChannel.Id, slackConfig.OAuthToken);

    if (topicConfig.IsDeadLetterQueue) {
        const attr = record.Sns.MessageAttributes
        message.attach([
            new TopicAttachment(
                'Error Details'
                , ''
                , 'danger'
                , `RequestID: ${attr.RequestID.Value}\n` +
                `ErrorCode: ${attr.ErrorCode.Value}\n` +
                `ErrorMessage: ${attr.ErrorMessage.Value}`
            )]);
    } else {

        try {
            const snsMessage: SnsMessage = JSON.parse(record.Sns.Message)
            console.log(JSON.stringify(snsMessage, null, 4));
            if (snsMessage.Errors) {
                message.attach(snsMessage.Errors.map(e => new TopicAttachment(e.title, e.message, 'danger', getDetails(e.details))));
                delete snsMessage.Errors;
            }
            if (snsMessage.Logs) {
                message.attach(snsMessage.Logs.map(e => new TopicAttachment(e.title, e.message, 'log', getDetails(e.details))))
                delete snsMessage.Logs;
            }

            /* If the message had other keys aside from Errors and Logs. */
            if (Object.keys(snsMessage).length > 0) {
                message.attach(new TopicAttachment("Details", "", "log", JSON.stringify(snsMessage, null, 4)))
            }
        } catch (e) {
            console.log(`Couldn't parse SNS message. ${e}`);
        }
    }

    await postSlackMessage(message);
    return `Relayed SNS notification from ${arn} to ${slackChannel.Name} (${slackChannel.Id})`;
}

function getDetails(details: string | object): string {
    if (typeof details == "object")
        return JSON.stringify(details);
    else
        return details;
}

async function GetSnsTopicSlackConfig(SnsTopicArn: string) {
    return new SnsTopicSlackConfig(
        (await getDynamoTableRow('SnsTopicSlackMappings', 'SnsTopicArn', SnsTopicArn)) as DynamoDB.AttributeMap
    );
}

export class SnsTopicSlackConfig {
    private _record: DynamoDB.AttributeMap;

    constructor(record: DynamoDB.AttributeMap) {
        this._record = record;
    }

    get Enabled() {
        return this._record.Enabled.BOOL as boolean;
    }

    get IsDeadLetterQueue() {
        if (this._record?.IsDeadLetterQueue?.BOOL) {
            return true;
        } else {
            return false;
        }
    }

    get SlackAppName() {
        return this._record.SlackAppName.S as string;
    }

    get SlackChannelName() {
        return this._record.SlackChannelName.S as string;
    }

    get SnsTopicArn() {
        return this._record.SnsTopicArn.S as string;
    }
}