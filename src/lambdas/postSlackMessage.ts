import { WebClient } from "@slack/client";
import { PostMessageArguments } from "../models/messages";

export async function postSlackMessage(event: PostMessageArguments, _: any, callback: any) {
    console.log("postSlackMessage. Event: " + JSON.stringify(event, null, 4));

    const result = await new WebClient(event.token).chat.postMessage(event)
        .then((data: any) => {
            console.log(`Message sent: ${JSON.stringify(data, null, 4)}`)
            return data;
        })
        .catch((error) => {
            console.error(error);
            return error;
        });

    callback(null, result);
}