
export class SlackEmptyResponse {
    statusCode = 200;
    constructor(){}
}

export class SlackTextResponse extends SlackEmptyResponse {
    headers = {
        'content-type': 'application/json'
    };

    body: string;    
    constructor(text: string) {
        super();
        this.body = JSON.stringify({
            text: text
        });
    }
}