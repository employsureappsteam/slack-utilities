import { MessageAttachment, ChatPostMessageArguments, ChatUpdateArguments } from "@slack/client";

export const CODE_BLOCK = "```"

export class PostMessageArguments implements ChatPostMessageArguments {
    attachments: MessageAttachment[];
    channel: string;
    token: string;
    text: string;

    constructor(text: string, channel: string, token: string) {
        this.text = text;
        this.channel = channel;
        this.token = token;
        this.attachments = [];
    }

    attach(att: MessageAttachment | MessageAttachment[]) {
        this.attachments = this.attachments.concat(att);
    }
}

/**
 * Meets requirements for chat.update argument.
 * https://api.slack.com/methods/chat.update
 */
export class UpdateMessageArguments implements ChatUpdateArguments {
    token: string;
    channel: string;
    text: string;
    ts: string;
    as_user?: boolean;
    attachments: MessageAttachment[];
    link_names?: boolean;
    parse?: 'full' | 'none';

    constructor(token: string, channel: string, text: string, ts: string) {
        this.token = token;
        this.channel = channel;
        this.text = text;
        this.ts = ts;

        // Don't post as user please.
        this.as_user = false;
        
        // Force removal of attachments. Default behaviour. 
        this.attachments = [];
    }

    attach(att: MessageAttachment | MessageAttachment[]) {
        this.attachments = this.attachments.concat(att);
    }
}