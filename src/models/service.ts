import { ActionPayload } from "./slackPayloads";
import { SlashCommandPayload } from "./slackPayloads";

export type SlashCommandHandler = (arg: SlashCommandPayload, config: SlackConfig) => Promise<{ success: boolean, message?: string }>;
export type ActionHandler = (arg: ActionPayload, config: SlackConfig) => Promise<{ success: boolean, message: string }>;
export interface SlackConfig {
    readonly SigningSecret: string;
    readonly OAuthToken: string;
}

export interface ISlashCommandService {
    ConfigPath: string;
    Execute: SlashCommandHandler;
    /* Optional - if undefined all channels allowed. */
    AllowedChannels?: Array<string>
}
export interface ActionService {
    ConfigPath: string;
    Execute: ActionHandler;
};

