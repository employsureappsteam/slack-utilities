import { AttachmentAction, OptionField } from "@slack/client";

/**
 * Action names to be 
 */
export const ActionNames = {
    Acknowledge: "acknowledge",
    Retry: "retry",
    Ping: "ping",
    EmployeeSelect: "employee_select",
    Run: "run",
    Cancel: "cancel",
    Confirm: "confirm",
    ProvisionWorkplace: "provisionWorkplace",
    UpdateWorkplaceAdmins: "updateWorkplaceAdmins"
};

export class DefaultButton implements AttachmentAction {
    /* Text on button */
    text: string;
    /* Identifies button in action response. */
    name: string;
    /* Anything. Can contain up to 2000 characters. */
    value: string;

    style: 'default' | 'primary' | 'danger' = 'default';
    type: 'select' | 'button' = 'button';

    constructor(name: string, text: string, value: string = "") {
        this.name = name;
        this.text = text;
        this.value = value;
    }
}

export class PrimaryButton extends DefaultButton {
    constructor(name: string, text: string, value: string = "") {
        super(name, text, value);
        this.style = 'primary';
    }
}

export class DangerButton extends DefaultButton {
    constructor(name: string, text: string, value: string = "") {
        super(name, text, value)
        this.style = 'danger';
    }
}

export class Selection implements AttachmentAction {
    name: string;
    text: string;
    value: string = "";
    type: 'select' | 'button' = 'select';
    options: OptionField[];

    constructor(name: string, text: string, options: OptionField[]) {
        this.name = name;
        this.text = text;
        this.options = options;
    }
}

