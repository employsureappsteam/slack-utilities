import { MessageAttachment, AttachmentAction, OptionField } from "@slack/client";
import { Selection, DefaultButton, ActionNames } from "./actions";
import { SlackMessageTypes } from "../constants/slack";
import { lookupMessageTypeColor } from "../libs/slack";
import { CODE_BLOCK } from "./messages";


export const CallbackIDs = {
    FailedSync: "PureCloudSyncFail",
    ProvisionSelection: "ProvisionEmployeeSelection",
    ProvisionConfirmation: "ProvisionEmployeeConfirmation",
    NZLeadScrapeConfirmation: "NZLeadScrapeConfirmation",
    FWCScrapeConfirmation: "FWCScrapeConfirmation",
    WorkplaceProvisioningAction: "ProvisionWorkplace",
    WorkplaceProvisionRetry: "WorkplaceProvisionRetry"
};

export class DefaultAttachment implements MessageAttachment {
    title: string = "";
    text: string = "";
    pretext: string = "";
    footer: string = "";

    /* Message displayed to users using an interface that does not support attachments or interactive messages */
    fallback: string = "";

    /* Color to distinguish attachment from others in same message */
    color: 'good' | 'warning' | 'danger' | string;

    /* Array of attachments to be set in the constructor. */
    actions: AttachmentAction[] = [];

    /* Set in constructor */
    callback_id: string;

    addAction(action: AttachmentAction) {
        this.actions.push(action);
    }

    removeAllActions() {
        this.actions = [];
    }

    appendText(text: string) {
        this.text = `${this.text}\n${text}`;
    }

    constructor(title: string, text: string, callback_id: string = "", notifyChannel: boolean = false, ) {
        this.title = title;
        this.text = text;
        this.color = lookupMessageTypeColor(SlackMessageTypes.Info);
        this.callback_id = callback_id;

        // Append channel notification to message title if requested
        if (notifyChannel) {
            this.text += " <!channel>";
        }
    }
}

export class TopicAttachment extends DefaultAttachment {
    constructor(title: string, message: string, color: string, details: string) {
        super(title, "");
        const detailsWrapped = details ? (CODE_BLOCK + "\n" + details + "\n" + CODE_BLOCK) : '';
        this.text = `${message}\n${detailsWrapped}`
        this.color = color;
    }
}

/**
 * Class for a failed sync job attachment.
 */
export class FailedMessageAttachement extends DefaultAttachment {
    constructor(title: string, callbackId: string, value: string) {
        super(title, "", callbackId, true)
        this.color = lookupMessageTypeColor(SlackMessageTypes.Failed);

        this.callback_id = callbackId;
        this.addAction(new DefaultButton("acknowledge", "Acknowledge", "acknowledge"));
        this.addAction(new DefaultButton("retry", "Retry Sync", value));
    }
}

export class ProvisionEmployeeSelection extends DefaultAttachment {
    constructor(text: string, emails: string[]) {
        super("Provision Request", text, CallbackIDs.ProvisionSelection, false);

        // Prepare emails into valid selection options for slack
        const options: OptionField[] = [];
        emails.map(e => {
            options.push({
                text: e,
                value: e
            })
        })

        this.addAction(new Selection(ActionNames.EmployeeSelect, "Employees", options));
    }
}