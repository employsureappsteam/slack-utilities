# Slack Utilities

This repo contains a library to help post messages to slack, and also the AWS Lambda functions used to integrate into slack. 

## Installation into a NodeJS project

Install with npm into your project:  
`npm install --save git+https://git@bitbucket.org/employsureappsteam/slack-utilities.git` 

## Publishing this package

To publish the repo so it can be installed into other NodeJS packages. With bash:

* Run `npm run build`
* Run `git add [your files]`
* Run `git commit -m "[commit message]"`
* Run `npm version [ major | minor | patch ]` to bump the version and publish to git. 

Run `npm update` in projects depending on this repo.

## Development

To develop on and deploy this project. 

Required: serverless, AWS CLI

1.  Install Serverless by executing `npm i -g serverless`.
2.  Install the AWS CLI downloading it from `https://docs.aws.amazon.com/cli/latest/userguide/awscli-install-windows.html` and install it.
3.  Configure AWS CLI by executing `aws configure`.

## New Slack Integration

A new slack app must be created and setup for it to work with the API.

### Create slack App:

1. Create slack app here: https://api.slack.com/apps
2. Under Add features and fucntionality, enable:
    * Slash Commands
        * Add slash commands as necessary (see below for more details)
    * Interactive Components
        * Set request URL to the slack/action endpoint (find in AWS console, or see log after deploying)
3. Set signing secret for Slack App in SSM  
`aws ssm put-parameter --name "/Slack/[AppName]/SigningSecret" --value "[secret]" --type SecureString`
4. Set OAuth Token for Slack App in SSM  
`aws ssm put-parameter --name "/Slack/[AppName]/OAuthToken" --value "[secret]" --type SecureString`

### To add a new slash command:
1. On your [Slack App](https://api.slack.com/apps), add a slash command
2. Set it's Request URL to the slack/action endpoint URL (find in AWS console, or see log after deploying)
3. In src/services, create a new file and export a SlashService object
4. In src/lambdas/slashHandler.ts
    * Import your SlashService
    * Add import to Commands object
    * Ensure "/command" string is identical to slash command created in Step 1.

## Deployment

Ensure you are running as administrator, then use `npm run deploy`.

## Setting up Slack Integration


1. Create a new Slack App (currently, `ThinkTank V2`)
2. In Slack App Dashboard, Navigate to `OAuth & Permissions` & `Install to Workspace` to acquire a `Bot User OAuth Token`
3. In `Scopes`, add the following `OAuth Scope`:
      1. channels:read
      2. chat:write
      3. chat:write.customize
      4. chat:write.public
      5. commands
4. "Reinstall to Workspace" as recommended by Slack App
5. In Slack App Dashboard, Note down both `OAuth Token` & `Basic Information`->`App Credentials`->`Signing Secret`
6. In AWS/Apigateway Dashboard, navigate to `dev-slack-utilities`
7. Navigate to `Stages`
8. In AWS/Apigateway Dashboard, within `Stages`, navigate to `/slack/action` POST. Note down the `Invoke URL`
9. In AWS/Apigateway Dashboard, navigate to `/slack/slash` POST. Note down the `Invoke URL`
10. In Slack App Dashboard, navigate to `Slash Commands`
11. In Slack App Dashboard, insert `src/lambdas/slashHandler.ts Commands` as New Command(s). 
12. Insert the `https://.../slack/slash` url as the `Request URL` for all commands.
13. In Slack App Dashboard, Navigate to `Interactivity & Shortcuts`
14. Turn `Interactivity` ON.
15. Insert the `https://.../slack/action` url as the `Request URL` for `Interactivity`
16. In Slack, right-click on `engineering-dev-errors` and `Open channel details`
17. In the `Apps` box, make sure `[Your-App]` (currently, `ThinkTank V2`) is added
18. Repeat for `engineering-prod-errors`


## Integration Verification & Testing

### Sns to Slack Messaging

1. In AWS/SNS Dashboard, search and find `engineering-errors-[prod|dev]`
2. `Publish message`
3. Ensure the appropriate Slack Channel receives the message

## Examples

### Example: Sending a message to development channel:
```typescript
import { postSlackMessage, PostMessageArguments, getSlackAppConfig, AppConfigPaths, ChannelIDs } from "slack-utilities"

export const postAMessageToDev = async (message: string) => {
    /* Get the config for a specific Slack App. */
    const config = await getSlackAppConfig(AppConfigPaths.Development);

    /* Create slack message with a text string, channel ID, and OAuthToken. */
    const slackMessage = new PostMessageArguments(
        message, 
        ChannelIDs.Development, 
        config.OAuthToken
    )

    /* Send message to lambda. */
    postSlackMessage(slackMessage);
}
```

### Example: Adding attachments / buttons to message:
```typescript
const slackMessage: new PostMessageArguments( ... );

/* Create an attachment. attachment-context-id is used to identify the 
* attachment by the lambda action handler if a Slack action is triggered. */
const attachment = new DefaultAttachment("A Title", "A message", "attachment-context-id");

/* Add an action (button or selection) to attachment. 
* name is used to identify what button was pressed.
* value is is passed back via the action handler. Can be anything up to 2000 characters. */
attachment.addAction(new DefaultButton("accept", "Accept", "action-value"));

/* Attach to message */
slackMessage.attach(attachment);
postSlackMessage(slackMessage);
```
