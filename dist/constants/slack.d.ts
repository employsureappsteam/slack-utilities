export declare enum SlackMessageTypes {
    Info = 0,
    Success = 1,
    Warning = 2,
    Failed = 3,
    Default = 4
}
