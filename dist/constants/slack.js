"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SlackMessageTypes = void 0;
var SlackMessageTypes;
(function (SlackMessageTypes) {
    SlackMessageTypes[SlackMessageTypes["Info"] = 0] = "Info";
    SlackMessageTypes[SlackMessageTypes["Success"] = 1] = "Success";
    SlackMessageTypes[SlackMessageTypes["Warning"] = 2] = "Warning";
    SlackMessageTypes[SlackMessageTypes["Failed"] = 3] = "Failed";
    SlackMessageTypes[SlackMessageTypes["Default"] = 4] = "Default";
})(SlackMessageTypes = exports.SlackMessageTypes || (exports.SlackMessageTypes = {}));
//# sourceMappingURL=slack.js.map