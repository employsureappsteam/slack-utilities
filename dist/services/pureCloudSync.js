"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.startPureCloudSync = exports.pureCloudFailedResponse = void 0;
var aws_sdk_1 = require("aws-sdk");
var slack_1 = require("../libs/slack");
var attachments_1 = require("../models/attachments");
var messages_1 = require("../models/messages");
var actions_1 = require("../models/actions");
var employsure_core_1 = require("employsure-core");
exports.pureCloudFailedResponse = {
    ConfigPath: employsure_core_1.AppConfigPaths.AWSNotificationService,
    Execute: actionPureCloudFailedResponse
};
exports.startPureCloudSync = {
    ConfigPath: employsure_core_1.AppConfigPaths.AWSNotificationService,
    Execute: slashStartPureCloudSync
};
function slashStartPureCloudSync(payload, config) {
    return __awaiter(this, void 0, void 0, function () {
        var method, user, origin, stepfunctions, stepFunctionParams;
        return __generator(this, function (_a) {
            method = payload.text;
            user = payload.user_name;
            origin = "Slack Slash Command";
            stepfunctions = new aws_sdk_1.StepFunctions({
                region: employsure_core_1.AWS_REGION_DEFAULT,
                apiVersion: employsure_core_1.AWS_STEP_FUNCTIONS_API_VERSION
            });
            stepFunctionParams = {
                stateMachineArn: employsure_core_1.SfArnMap.syncPureCloudData,
                input: JSON.stringify({
                    pureCloudExecution: {
                        method: method
                    }
                }),
                name: user + "-Retry_" + method + "-" + Date.now(),
            };
            // Initiate pure cloud sync step function and post result to slack
            stepfunctions.startExecution(stepFunctionParams, function (error, data) {
                var slackMessage = new messages_1.PostMessageArguments("[" + origin + "] " + method + " triggered by " + user, employsure_core_1.ChannelIDs.PureCloudSync, config.OAuthToken);
                if (error) {
                    slackMessage.attach(new attachments_1.FailedMessageAttachement("Failed to execute. Error: " + error.message, attachments_1.CallbackIDs.FailedSync, "retry"));
                }
                slack_1.postSlackMessage(slackMessage);
            });
            return [2 /*return*/, { success: true, message: "Pure cloud sync started" }];
        });
    });
}
/**
 * Handle fail attachment for pure cloud sync failing.
 *
 * Will be either acknolwedge or retry.
 *
 * @param user Name of user invoking
 * @param method Method name to pass to sync
 * @param origin Where you are calling this from (e.g. slash command, retry button)
 */
function actionPureCloudFailedResponse(payload, config) {
    return __awaiter(this, void 0, void 0, function () {
        var origin, user, action, messageValue, method, stepfunctions, stepFunctionParams;
        return __generator(this, function (_a) {
            origin = "Fail attachment";
            user = payload.user.name;
            action = payload.actions[0].name;
            messageValue = payload.actions[0].value;
            method = messageValue.pureCloudExecution.method;
            if (action === actions_1.ActionNames.Acknowledge) {
                slack_1.postSlackMessage(new messages_1.PostMessageArguments(user + " has acknowledge error. Method: " + method + ".", employsure_core_1.ChannelIDs.PureCloudSync, config.OAuthToken));
            }
            else if (action === actions_1.ActionNames.Retry) {
                stepfunctions = new aws_sdk_1.StepFunctions({
                    region: employsure_core_1.AWS_REGION_DEFAULT,
                    apiVersion: employsure_core_1.AWS_STEP_FUNCTIONS_API_VERSION
                });
                stepFunctionParams = {
                    stateMachineArn: employsure_core_1.SfArnMap.syncPureCloudData,
                    input: JSON.stringify({
                        pureCloudExecution: {
                            method: method
                        }
                    }),
                    name: user + "-Retry_" + method + "-" + Date.now(),
                };
                // Initiate pure cloud sync step function and post result to slack
                stepfunctions.startExecution(stepFunctionParams, function (error, data) {
                    var slackMessage = new messages_1.PostMessageArguments("[" + origin + "] " + method + " triggered by " + user, employsure_core_1.ChannelIDs.PureCloudSync, config.OAuthToken);
                    if (error) {
                        slackMessage.attach(new attachments_1.FailedMessageAttachement("Failed to execute. Error: " + error.message, attachments_1.CallbackIDs.FailedSync, "retry"));
                    }
                    slack_1.postSlackMessage(slackMessage);
                });
                slack_1.postSlackMessage(new messages_1.PostMessageArguments(user + " has retriggered a retry for " + method + ".", employsure_core_1.ChannelIDs.PureCloudSync, config.OAuthToken));
            }
            return [2 /*return*/, { success: true, message: "started" }];
        });
    });
}
//# sourceMappingURL=pureCloudSync.js.map