"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.confirmStartFWCScrape = exports.requestStartFWCScrape = void 0;
var messages_1 = require("../models/messages");
var slack_1 = require("../libs/slack");
var employsure_core_1 = require("employsure-core");
var lambda_1 = require("../aws/lambda");
var actions_1 = require("../models/actions");
var attachments_1 = require("../models/attachments");
exports.requestStartFWCScrape = {
    ConfigPath: employsure_core_1.AppConfigPaths.Development,
    Execute: function (payload, config) { return __awaiter(void 0, void 0, void 0, function () {
        var message, attachment, runButtonValue, value;
        return __generator(this, function (_a) {
            if (payload.channel_id === employsure_core_1.ChannelIDs.Engineering) {
                message = new messages_1.PostMessageArguments('', employsure_core_1.ChannelIDs.Engineering, config.OAuthToken);
                attachment = new attachments_1.DefaultAttachment('FWC Scrape', "Please confirm that you want to run FWC Scrape.", attachments_1.CallbackIDs.FWCScrapeConfirmation);
                runButtonValue = { PermittedUserId: payload.user_id };
                value = JSON.stringify(runButtonValue);
                attachment.addAction(new actions_1.PrimaryButton(actions_1.ActionNames.Run, 'Run', value));
                attachment.addAction(new actions_1.DefaultButton(actions_1.ActionNames.Cancel, 'Cancel', value));
                message.attach(attachment);
                slack_1.postSlackMessage(message);
                return [2 /*return*/, { success: true, message: payload.user_name + " has requested FWC scrape." }];
            }
            else {
                return [2 /*return*/, { success: false, message: "Command must be run from the Engineering channel. (from <#" + employsure_core_1.ChannelIDs.Engineering + ">)" }];
            }
            return [2 /*return*/];
        });
    }); }
};
/**
 * Handle an
 */
exports.confirmStartFWCScrape = {
    ConfigPath: employsure_core_1.AppConfigPaths.Development,
    Execute: function (payload, config) { return __awaiter(void 0, void 0, void 0, function () {
        var actionName, value, message;
        return __generator(this, function (_a) {
            actionName = payload.actions[0].name;
            value = JSON.parse(payload.actions[0].value);
            if (value.PermittedUserId !== payload.user.id) {
                return [2 /*return*/, { success: false, message: "You are not permitted to use this action." }];
            }
            message = new messages_1.UpdateMessageArguments(config.OAuthToken, payload.channel.id, '', payload.message_ts);
            // TODO: Fix this so message is only updated once the lambda invokes successfully...
            // Handle which button was pressed
            if (actionName === actions_1.ActionNames.Run) {
                message.attach(new attachments_1.DefaultAttachment("FWC Scrape", ":heavy_check_mark: FWC Scrape has started."));
                lambda_1.invokeLambda(employsure_core_1.LambdaNamesMap.FWCLeadScrapper, {})
                    .then(function (data) {
                    slack_1.postSlackMessage(new messages_1.PostMessageArguments("<@" + payload.user.id + "> Started FWC Scrape.", employsure_core_1.ChannelIDs.EngAlertsDev, config.OAuthToken));
                })
                    .catch(function (error) {
                    slack_1.postMessageToDevelopment(new attachments_1.DefaultAttachment('Failed to invoke FWC Scrape', "<@" + payload.user.id + "> tried to start FWC Scrape. Failed:\n" + messages_1.CODE_BLOCK + error + messages_1.CODE_BLOCK), 'error');
                    console.log("Error:", error);
                });
            }
            else if (actionName === actions_1.ActionNames.Cancel) {
                message.attach(new attachments_1.DefaultAttachment("FWC Scrape", ":negative_squared_cross_mark: FWC was not started."));
            }
            slack_1.updateSlackMessage(message);
            return [2 /*return*/, { success: true, message: "FWC confirmation handled." }];
        });
    }); }
};
//# sourceMappingURL=fwcScraping.js.map