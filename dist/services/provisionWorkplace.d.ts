import { ISlashCommandService, ActionService } from "../models/service";
export declare const invokeWorkplaceProvisioning: ISlashCommandService;
export declare const confirmWorkplaceProvisioning: ActionService;
/**
 * To do with retry events. The message can be posted from anywhere.
 * To trigger this action the action must have the callback id as mapped in actionHandler.ts.
 * In this case, CallbackIDs.WorkplaceProvisionRetry
 */
export declare const retryProvisionWorkplace: ActionService;
