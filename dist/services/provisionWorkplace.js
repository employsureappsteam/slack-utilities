"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.retryProvisionWorkplace = exports.confirmWorkplaceProvisioning = exports.invokeWorkplaceProvisioning = void 0;
var messages_1 = require("../models/messages");
var lambda_1 = require("../aws/lambda");
var employsure_core_1 = require("employsure-core");
var actions_1 = require("../models/actions");
var attachments_1 = require("../models/attachments");
var slack_1 = require("../libs/slack");
exports.invokeWorkplaceProvisioning = {
    ConfigPath: employsure_core_1.AppConfigPaths.Provisioning,
    Execute: function (payload, config) { return __awaiter(void 0, void 0, void 0, function () {
        var arg, stage, title, message, attachment, runButtonValue, buttonValue;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    arg = payload.text;
                    if (!(payload.channel_id !== employsure_core_1.ChannelIDs.Provisioning && payload.channel_id !== employsure_core_1.ChannelIDs.EngAlertsDev)) return [3 /*break*/, 1];
                    /* Can be secretly run from #eng-alerts-dev */
                    return [2 /*return*/, { success: false, message: payload.command + " can only be run from #provisioning." }];
                case 1:
                    if (!(payload.channel_id === employsure_core_1.ChannelIDs.EngAlertsDev && arg !== "Dev" && arg !== "Prod")) return [3 /*break*/, 2];
                    return [2 /*return*/, { success: false, message: "Failed to run " + payload.command + ". Incorrect araguments. Format: \n`" + payload.command + " [\"Dev\" | \"Prod\"]`" }];
                case 2:
                    stage = payload.channel_id === employsure_core_1.ChannelIDs.Provisioning ? "Prod" : arg;
                    title = payload.channel_id === employsure_core_1.ChannelIDs.Provisioning ? "Workplace Provisioning" : "Workplace Provisioning [" + arg + "]";
                    message = new messages_1.PostMessageArguments('', payload.channel_id, config.OAuthToken);
                    attachment = new attachments_1.DefaultAttachment(title, "<@" + payload.user_name + "> please choose one of the following actions.", attachments_1.CallbackIDs.WorkplaceProvisioningAction);
                    runButtonValue = { PermittedUserId: payload.user_id, Stage: stage };
                    buttonValue = JSON.stringify(runButtonValue);
                    attachment.addAction(new actions_1.PrimaryButton(actions_1.ActionNames.ProvisionWorkplace, 'Provision Workplace', buttonValue));
                    if (payload.channel_id === employsure_core_1.ChannelIDs.EngAlertsDev) {
                        // Also attach button to update admins if run from secret eng channel
                        attachment.addAction(new actions_1.PrimaryButton(actions_1.ActionNames.UpdateWorkplaceAdmins, 'Update Workplace Admins', buttonValue));
                    }
                    attachment.addAction(new actions_1.DefaultButton(actions_1.ActionNames.Cancel, 'Cancel', buttonValue));
                    message.attach(attachment);
                    return [4 /*yield*/, lambda_1.invokeLambda(employsure_core_1.LambdaNamesMap.postSlackMessage, message)];
                case 3:
                    _a.sent();
                    console.log("Sent confirmation to user " + payload.user_name + " in channel " + payload.channel_name + " (" + payload.channel_id + ") for stage " + arg + ".");
                    return [2 /*return*/, { success: true }];
            }
        });
    }); }
};
exports.confirmWorkplaceProvisioning = {
    ConfigPath: employsure_core_1.AppConfigPaths.Provisioning,
    Execute: function (payload, config) { return __awaiter(void 0, void 0, void 0, function () {
        var actionName, _a, PermittedUserId, Stage, lambdaName, title, message, eventPayload, text, doInvoke;
        return __generator(this, function (_b) {
            switch (_b.label) {
                case 0:
                    actionName = payload.actions[0].name;
                    _a = JSON.parse(payload.actions[0].value), PermittedUserId = _a.PermittedUserId, Stage = _a.Stage;
                    if (PermittedUserId !== payload.user.id) {
                        return [2 /*return*/, { success: false, message: "You don't have permission to use this." }];
                    }
                    if (Stage !== "Dev" && Stage !== "Prod") {
                        return [2 /*return*/, { success: false, message: "Invalid stage " + Stage + "." }];
                    }
                    lambdaName = "provisioning-" + Stage.toLowerCase() + "-provisionWorkplace";
                    title = Stage === "Dev" ? "Dev | Workplace Provisioning" : "Workplace Provisioning";
                    message = new messages_1.UpdateMessageArguments(config.OAuthToken, payload.channel.id, '', payload.message_ts);
                    eventPayload = {
                        Author: payload.user.name,
                        Stage: Stage,
                        Source: 'emp.slack',
                        options: {
                            provisionUsers: true,
                            updateUsers: true,
                            updateGroupMembers: true,
                            updateGroupAdmins: false,
                        }
                    };
                    text = '';
                    doInvoke = false;
                    if (actionName === actions_1.ActionNames.ProvisionWorkplace) {
                        doInvoke = true;
                        text = ":heavy_check_mark: Workplace provisioning started by <@" + payload.user.name + ">.";
                    }
                    else if (actionName === actions_1.ActionNames.UpdateWorkplaceAdmins) {
                        eventPayload.options.provisionUsers = false;
                        eventPayload.options.updateUsers = false;
                        eventPayload.options.updateGroupAdmins = false;
                        eventPayload.options.updateGroupAdmins = true;
                        doInvoke = true;
                        text = ":heavy_check_mark: Workplace group admin update started by <@" + payload.user.name + ">.";
                    }
                    else if (actionName === actions_1.ActionNames.Cancel) {
                        text = ":negative_squared_cross_mark: Workplace provisioning cancelled by <@" + payload.user.name + ">.";
                    }
                    if (!doInvoke) return [3 /*break*/, 2];
                    return [4 /*yield*/, lambda_1.invokeLambda(lambdaName, eventPayload)];
                case 1:
                    _b.sent();
                    _b.label = 2;
                case 2:
                    message.attach(new attachments_1.DefaultAttachment(title, text));
                    slack_1.updateSlackMessage(message);
                    return [2 /*return*/, { success: true, message: "Workplace Provision Handled. action: " + actionName + ", stage: " + Stage }];
            }
        });
    }); }
};
/**
 * To do with retry events. The message can be posted from anywhere.
 * To trigger this action the action must have the callback id as mapped in actionHandler.ts.
 * In this case, CallbackIDs.WorkplaceProvisionRetry
 */
exports.retryProvisionWorkplace = {
    ConfigPath: employsure_core_1.AppConfigPaths.Provisioning,
    Execute: function (payload, config) { return __awaiter(void 0, void 0, void 0, function () {
        var actionName, _a, Stage, RetryCount, options, message, lambda, titlePrefix, eventPayload, title;
        return __generator(this, function (_b) {
            actionName = payload.actions[0].name;
            _a = JSON.parse(payload.actions[0].value), Stage = _a.Stage, RetryCount = _a.RetryCount, options = _a.options;
            message = new messages_1.UpdateMessageArguments(config.OAuthToken, payload.channel.id, '', payload.message_ts);
            if (Stage === undefined ||
                RetryCount === undefined ||
                options === undefined) {
                slack_1.updateSlackMessage(new messages_1.UpdateMessageArguments(config.OAuthToken, payload.channel.id, "Failed to retry workplace provisioning", payload.message_ts));
                return [2 /*return*/, { success: false, message: "Can't retry workplace. Missing button parameters." }];
            }
            lambda = Stage === "Prod" ? employsure_core_1.LambdaNamesMap.ProvisionWorkplaceProd : employsure_core_1.LambdaNamesMap.ProvisionWorkplaceDev;
            titlePrefix = Stage === "Prod" ? "" : "Dev | ";
            eventPayload = {
                Source: 'emp.slack',
                Author: "" + payload.user.name,
                RetryCount: RetryCount + 1,
                Stage: Stage,
                options: options
            };
            title = titlePrefix + "Workplace Provisioning finished with errors";
            if (actionName === actions_1.ActionNames.Cancel) {
                message.attach(new attachments_1.DefaultAttachment(title, ":negative_squared_cross_mark: Workplace provisioning retry cancelled."));
            }
            else if (actionName === actions_1.ActionNames.ProvisionWorkplace) {
                lambda_1.invokeLambda(lambda, eventPayload);
                message.attach(new attachments_1.DefaultAttachment(title, ":heavy_check_mark: Retrying Workplace provisioning."));
            }
            else {
                message.attach(new attachments_1.DefaultAttachment(title, ":negative_squared_cross_mark: Something went wrong. :negative_squared_cross_mark:"));
            }
            slack_1.updateSlackMessage(message);
            return [2 /*return*/, { success: true, message: "Workplace Provision Handled. action: " + actionName + ", stage: " + Stage }];
        });
    }); }
};
//# sourceMappingURL=provisionWorkplace.js.map