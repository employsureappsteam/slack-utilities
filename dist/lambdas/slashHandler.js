"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.slashHandler = void 0;
var callbackResults_1 = require("../models/callbackResults");
// Libs
var slack_1 = require("../libs/slack");
var ssm_1 = require("../aws/ssm");
// Services
var pureCloudSync_1 = require("../services/pureCloudSync");
var fwcScraping_1 = require("../services/fwcScraping");
var nzLeadSearch_1 = require("../services/nzLeadSearch");
var provisionWorkplace_1 = require("../services/provisionWorkplace");
var messages_1 = require("../models/messages");
var attachments_1 = require("../models/attachments");
var syncPingboard_1 = require("../services/syncPingboard");
var syncSpinify_1 = require("../services/syncSpinify");
// Add slash commands here, in the form of "<slash command name>: <function to be run>"
var Commands = {
    "no function yet": pureCloudSync_1.startPureCloudSync,
    "/fwcscrape": fwcScraping_1.requestStartFWCScrape,
    "/nzleadscrape": nzLeadSearch_1.requestNZLeadScrape,
    "/provisionworkplace": provisionWorkplace_1.invokeWorkplaceProvisioning,
    "/syncpingboard": syncPingboard_1.syncPingboard,
    "/syncspinify": syncSpinify_1.syncSpinify
};
var slashHandler = function (event) { return __awaiter(void 0, void 0, void 0, function () {
    var slashPayload;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                console.log("slashHandler triggered. Event: " + JSON.stringify(event, null, 4));
                slashPayload = slack_1.parseSlashPayload(event.body);
                console.log("payload: " + JSON.stringify(slashPayload, null, 4));
                return [4 /*yield*/, handleSlackPayload(event, slashPayload)];
            case 1:
                _a.sent();
                return [2 /*return*/, new callbackResults_1.SlackEmptyResponse()];
        }
    });
}); };
exports.slashHandler = slashHandler;
function handleSlackPayload(request, slashPayload) {
    return __awaiter(this, void 0, void 0, function () {
        var command, config, allowedChannels, result, error_1;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    command = Commands[slashPayload.command];
                    if (!(command === undefined)) return [3 /*break*/, 2];
                    return [4 /*yield*/, slack_1.postMessageToDevelopment(new attachments_1.DefaultAttachment(slashPayload.command + " does not exist.", "" + messages_1.CODE_BLOCK + JSON.stringify(slashPayload, null, 4) + messages_1.CODE_BLOCK), 'error')];
                case 1:
                    _a.sent();
                    return [2 /*return*/];
                case 2: return [4 /*yield*/, ssm_1.getSlackAppConfig(command.ConfigPath)];
                case 3:
                    config = _a.sent();
                    if (!!slack_1.VerifySlackSignature(request, config.SigningSecret)) return [3 /*break*/, 5];
                    return [4 /*yield*/, slack_1.postMessageToDevelopment(new attachments_1.DefaultAttachment('Cannot verify slack signature.', "" + messages_1.CODE_BLOCK + JSON.stringify(slashPayload, null, 4) + messages_1.CODE_BLOCK), 'error')];
                case 4:
                    _a.sent();
                    return [2 /*return*/];
                case 5:
                    allowedChannels = command.AllowedChannels;
                    if (!(allowedChannels !== undefined && !allowedChannels.includes(slashPayload.channel_id))) return [3 /*break*/, 7];
                    return [4 /*yield*/, slack_1.postEphemeralMessage(slashPayload.user_id, new messages_1.PostMessageArguments(slashPayload.command + " cannot be run from this channel.", slashPayload.channel_id, config.OAuthToken))];
                case 6:
                    _a.sent();
                    return [2 /*return*/];
                case 7:
                    _a.trys.push([7, 11, , 13]);
                    return [4 /*yield*/, command.Execute(slashPayload, config)];
                case 8:
                    result = _a.sent();
                    if (!result.message) return [3 /*break*/, 10];
                    return [4 /*yield*/, slack_1.postEphemeralMessage(slashPayload.user_id, new messages_1.PostMessageArguments(result.message, slashPayload.channel_id, config.OAuthToken))];
                case 9:
                    _a.sent();
                    _a.label = 10;
                case 10: return [3 /*break*/, 13];
                case 11:
                    error_1 = _a.sent();
                    console.log(error_1);
                    return [4 /*yield*/, slack_1.postEphemeralMessage(slashPayload.user_id, new messages_1.PostMessageArguments("Server Error. Please contact Engineering.", slashPayload.channel_id, config.OAuthToken))];
                case 12:
                    _a.sent();
                    return [3 /*break*/, 13];
                case 13: return [2 /*return*/];
            }
        });
    });
}
//# sourceMappingURL=slashHandler.js.map