import { Handler } from "aws-lambda";
import { DynamoDB } from "aws-sdk";
export declare const snsPublishToSlack: Handler;
export declare class SnsTopicSlackConfig {
    private _record;
    constructor(record: DynamoDB.AttributeMap);
    get Enabled(): boolean;
    get IsDeadLetterQueue(): boolean;
    get SlackAppName(): string;
    get SlackChannelName(): string;
    get SnsTopicArn(): string;
}
