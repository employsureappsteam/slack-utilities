import { PostMessageArguments } from "../models/messages";
export declare function postSlackMessage(event: PostMessageArguments, _: any, callback: any): Promise<void>;
