"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SnsTopicSlackConfig = exports.snsPublishToSlack = void 0;
var ssm_1 = require("../aws/ssm");
var messages_1 = require("../models/messages");
var attachments_1 = require("../models/attachments");
var slack_1 = require("../libs/slack");
var employsure_core_1 = require("employsure-core");
var snsPublishToSlack = function (event) { return __awaiter(void 0, void 0, void 0, function () {
    var resultMessage, record, isFromSqs, isFromSns, error_1;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                resultMessage = undefined;
                _a.label = 1;
            case 1:
                _a.trys.push([1, 7, , 8]);
                if (!event.Records) {
                    console.log("event: " + JSON.stringify(event, null, 4));
                    throw new Error("Event has no records.");
                }
                record = event.Records[0];
                isFromSqs = event.Records[0].eventSource === "aws:sqs";
                isFromSns = event.Records[0].EventSource === "aws:sns";
                if (!isFromSqs) return [3 /*break*/, 3];
                return [4 /*yield*/, sendFromSqs(record)];
            case 2:
                resultMessage = _a.sent();
                return [3 /*break*/, 6];
            case 3:
                if (!isFromSns) return [3 /*break*/, 5];
                return [4 /*yield*/, sendFromSns(record)];
            case 4:
                resultMessage = _a.sent();
                return [3 /*break*/, 6];
            case 5:
                console.log("event: " + JSON.stringify(event, null, 4));
                throw new Error("This lambda can only be invoked by SQS or SNS.");
            case 6: return [3 /*break*/, 8];
            case 7:
                error_1 = _a.sent();
                resultMessage = error_1.name + ": " + error_1.message;
                return [3 /*break*/, 8];
            case 8:
                console.log(resultMessage);
                return [2 /*return*/];
        }
    });
}); };
exports.snsPublishToSlack = snsPublishToSlack;
/** Assume this record is from a DLQ. */
function sendFromSqs(record) {
    return __awaiter(this, void 0, void 0, function () {
        var arn, topicConfig, slackChannel, slackConfig, messageSenderId, subject, title, text, attributeLog, details, message, attachment;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    arn = record.eventSourceARN;
                    return [4 /*yield*/, GetSnsTopicSlackConfig(arn)];
                case 1:
                    topicConfig = _a.sent();
                    return [4 /*yield*/, employsure_core_1.GetSlackChannelID(topicConfig.SlackChannelName)];
                case 2:
                    slackChannel = _a.sent();
                    if (!slackChannel.Id) {
                        throw new Error("SlackChannel missing Id");
                    }
                    if (!topicConfig.Enabled) {
                        return [2 /*return*/, "Slack notifications for " + topicConfig.SnsTopicArn + " are disabled."];
                    }
                    return [4 /*yield*/, ssm_1.getSlackAppConfig('/Slack/' + topicConfig.SlackAppName)];
                case 3:
                    slackConfig = _a.sent();
                    messageSenderId = record.attributes.SenderId;
                    subject = "AWS Dead Letter Received";
                    title = "Failed to process message sent by " + messageSenderId;
                    text = "See logs for the lambda that received the original message to see why it failed.";
                    attributeLog = Object.entries(record.attributes).map(function (_a) {
                        var k = _a[0], v = _a[1];
                        return k + ": " + v;
                    });
                    attributeLog.push("MessageId: " + record.messageId);
                    details = attributeLog.join("\n");
                    message = new messages_1.PostMessageArguments(subject, slackChannel.Id, slackConfig.OAuthToken);
                    attachment = new attachments_1.TopicAttachment(title, text, 'danger', details);
                    message.attach([attachment]);
                    return [4 /*yield*/, slack_1.postSlackMessage(message)];
                case 4:
                    _a.sent();
                    return [2 /*return*/, "Relayed SQS message from " + arn + " to " + slackChannel.Name];
            }
        });
    });
}
/* Handle a SNS record will well definied attributes. */
function sendFromSns(record) {
    return __awaiter(this, void 0, void 0, function () {
        var sns, arn, topicConfig, slackChannel, slackConfig, subject, snsTopicName, message, attr, snsMessage;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    sns = record.Sns;
                    arn = sns.TopicArn;
                    console.log("Sns: " + JSON.stringify(sns, null, 4));
                    return [4 /*yield*/, GetSnsTopicSlackConfig(arn)];
                case 1:
                    topicConfig = _a.sent();
                    if (!topicConfig) {
                        throw new Error("Cannot find Sns Topic Config for " + arn);
                    }
                    return [4 /*yield*/, employsure_core_1.GetSlackChannelID(topicConfig.SlackChannelName)];
                case 2:
                    slackChannel = _a.sent();
                    if (!slackChannel || !slackChannel.Id) {
                        throw new Error("Cannot find Slack Channel ID for " + topicConfig.SlackChannelName);
                    }
                    if (topicConfig.Enabled === false) {
                        return [2 /*return*/, topicConfig.SnsTopicArn + " slack notifications are disabled."];
                    }
                    return [4 /*yield*/, ssm_1.getSlackAppConfig('/Slack/' + topicConfig.SlackAppName)];
                case 3:
                    slackConfig = _a.sent();
                    subject = sns.Subject;
                    if (topicConfig.IsDeadLetterQueue) {
                        snsTopicName = arn.split(':')[5];
                        subject = "DLQ Notification (from SNS " + snsTopicName + ")";
                    }
                    message = new messages_1.PostMessageArguments(subject, slackChannel.Id, slackConfig.OAuthToken);
                    if (topicConfig.IsDeadLetterQueue) {
                        attr = record.Sns.MessageAttributes;
                        message.attach([
                            new attachments_1.TopicAttachment('Error Details', '', 'danger', "RequestID: " + attr.RequestID.Value + "\n" +
                                ("ErrorCode: " + attr.ErrorCode.Value + "\n") +
                                ("ErrorMessage: " + attr.ErrorMessage.Value))
                        ]);
                    }
                    else {
                        try {
                            snsMessage = JSON.parse(record.Sns.Message);
                            console.log(JSON.stringify(snsMessage, null, 4));
                            if (snsMessage.Errors) {
                                message.attach(snsMessage.Errors.map(function (e) { return new attachments_1.TopicAttachment(e.title, e.message, 'danger', getDetails(e.details)); }));
                                delete snsMessage.Errors;
                            }
                            if (snsMessage.Logs) {
                                message.attach(snsMessage.Logs.map(function (e) { return new attachments_1.TopicAttachment(e.title, e.message, 'log', getDetails(e.details)); }));
                                delete snsMessage.Logs;
                            }
                            /* If the message had other keys aside from Errors and Logs. */
                            if (Object.keys(snsMessage).length > 0) {
                                message.attach(new attachments_1.TopicAttachment("Details", "", "log", JSON.stringify(snsMessage, null, 4)));
                            }
                        }
                        catch (e) {
                            console.log("Couldn't parse SNS message. " + e);
                        }
                    }
                    return [4 /*yield*/, slack_1.postSlackMessage(message)];
                case 4:
                    _a.sent();
                    return [2 /*return*/, "Relayed SNS notification from " + arn + " to " + slackChannel.Name + " (" + slackChannel.Id + ")"];
            }
        });
    });
}
function getDetails(details) {
    if (typeof details == "object")
        return JSON.stringify(details);
    else
        return details;
}
function GetSnsTopicSlackConfig(SnsTopicArn) {
    return __awaiter(this, void 0, void 0, function () {
        var _a;
        return __generator(this, function (_b) {
            switch (_b.label) {
                case 0:
                    _a = SnsTopicSlackConfig.bind;
                    return [4 /*yield*/, employsure_core_1.getDynamoTableRow('SnsTopicSlackMappings', 'SnsTopicArn', SnsTopicArn)];
                case 1: return [2 /*return*/, new (_a.apply(SnsTopicSlackConfig, [void 0, (_b.sent())]))()];
            }
        });
    });
}
var SnsTopicSlackConfig = /** @class */ (function () {
    function SnsTopicSlackConfig(record) {
        this._record = record;
    }
    Object.defineProperty(SnsTopicSlackConfig.prototype, "Enabled", {
        get: function () {
            return this._record.Enabled.BOOL;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(SnsTopicSlackConfig.prototype, "IsDeadLetterQueue", {
        get: function () {
            var _a, _b;
            if ((_b = (_a = this._record) === null || _a === void 0 ? void 0 : _a.IsDeadLetterQueue) === null || _b === void 0 ? void 0 : _b.BOOL) {
                return true;
            }
            else {
                return false;
            }
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(SnsTopicSlackConfig.prototype, "SlackAppName", {
        get: function () {
            return this._record.SlackAppName.S;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(SnsTopicSlackConfig.prototype, "SlackChannelName", {
        get: function () {
            return this._record.SlackChannelName.S;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(SnsTopicSlackConfig.prototype, "SnsTopicArn", {
        get: function () {
            return this._record.SnsTopicArn.S;
        },
        enumerable: false,
        configurable: true
    });
    return SnsTopicSlackConfig;
}());
exports.SnsTopicSlackConfig = SnsTopicSlackConfig;
//# sourceMappingURL=snsPublishToSlack.js.map