"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var _a;
Object.defineProperty(exports, "__esModule", { value: true });
exports.actionHandler = void 0;
var slack_1 = require("../libs/slack");
var callbackResults_1 = require("../models/callbackResults");
var ssm_1 = require("../aws/ssm");
// Services
var pureCloudSync_1 = require("../services/pureCloudSync");
var messages_1 = require("../models/messages");
var nzLeadSearch_1 = require("../services/nzLeadSearch");
var attachments_1 = require("../models/attachments");
var fwcScraping_1 = require("../services/fwcScraping");
var provisionWorkplace_1 = require("../services/provisionWorkplace");
// Add slash commands here, in the form of "<slash command name>: <function to be run>"
var Actions = (_a = {},
    _a[attachments_1.CallbackIDs.FailedSync] = pureCloudSync_1.pureCloudFailedResponse,
    _a[attachments_1.CallbackIDs.NZLeadScrapeConfirmation] = nzLeadSearch_1.confirmNZLeadSearch,
    _a[attachments_1.CallbackIDs.FWCScrapeConfirmation] = fwcScraping_1.confirmStartFWCScrape,
    _a[attachments_1.CallbackIDs.WorkplaceProvisioningAction] = provisionWorkplace_1.confirmWorkplaceProvisioning,
    _a[attachments_1.CallbackIDs.WorkplaceProvisionRetry] = provisionWorkplace_1.retryProvisionWorkplace,
    _a);
var actionHandler = function (event, context, callback) { return __awaiter(void 0, void 0, void 0, function () {
    var payloadString, payloadJSON;
    return __generator(this, function (_a) {
        console.log("actionHandler triggered. Event: " + JSON.stringify(event, null, 4));
        payloadString = decodeURIComponent(event.body).replace("payload=", "");
        payloadJSON = JSON.parse(payloadString);
        console.log("payload: " + JSON.stringify(payloadJSON, null, 4));
        handleActionPayload(event, payloadJSON);
        callback(null, new callbackResults_1.SlackEmptyResponse());
        return [2 /*return*/];
    });
}); };
exports.actionHandler = actionHandler;
function handleActionPayload(request, actionPayload) {
    return __awaiter(this, void 0, void 0, function () {
        var action, config, result;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    if (!Actions[actionPayload.callback_id]) {
                        slack_1.postMessageToDevelopment(new attachments_1.DefaultAttachment("Slack Action " + actionPayload.callback_id + " does not exist", "Couldn't find an action for callback id `" + actionPayload.callback_id + "`, slack action payload is below:\n" + messages_1.CODE_BLOCK + JSON.stringify(actionPayload, null, 4) + messages_1.CODE_BLOCK), 'error');
                        return [2 /*return*/];
                    }
                    action = Actions[actionPayload.callback_id];
                    return [4 /*yield*/, ssm_1.getSlackAppConfig(action.ConfigPath)];
                case 1:
                    config = _a.sent();
                    if (!slack_1.VerifySlackSignature(request, config.SigningSecret)) {
                        slack_1.postMessageToDevelopment(new attachments_1.DefaultAttachment('Cannot verify slack signature.', "" + messages_1.CODE_BLOCK + JSON.stringify(actionPayload, null, 4) + messages_1.CODE_BLOCK), 'error');
                        return [2 /*return*/];
                    }
                    return [4 /*yield*/, action.Execute(actionPayload, config)];
                case 2:
                    result = _a.sent();
                    if (!(result.success === false)) return [3 /*break*/, 4];
                    return [4 /*yield*/, slack_1.postEphemeralMessage(actionPayload.user.id, new messages_1.PostMessageArguments(result.message, actionPayload.channel.id, config.OAuthToken))];
                case 3:
                    _a.sent();
                    return [3 /*break*/, 5];
                case 4:
                    console.log("Slash command finished: " + result.message);
                    _a.label = 5;
                case 5: return [2 /*return*/];
            }
        });
    });
}
//# sourceMappingURL=actionHandler.js.map