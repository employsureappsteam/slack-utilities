import { UpdateMessageArguments } from "../models/messages";
export declare function updateSlackMessage(event: UpdateMessageArguments, context: any, callback: any): Promise<void>;
