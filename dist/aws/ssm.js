"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.putParameter = exports.getParameters = exports.getParameter = exports.getSlackAppConfig = void 0;
var aws_sdk_1 = require("aws-sdk");
var employsure_core_1 = require("employsure-core");
;
var ssm = new aws_sdk_1.SSM({
    region: employsure_core_1.AWS_REGION_DEFAULT,
    apiVersion: employsure_core_1.AWS_SSM_API_VERSION
});
var SlackAppConfigParams = {
    SigningSecret: "/SigningSecret",
    OAuthToken: "/OAuthToken"
};
/**
 * Get signing secret from SSM
 * @param token App Vertification Token
 */
function getSlackAppConfig(rootPath) {
    return new Promise(function (resolve, reject) {
        // SSM get parameters
        var ssmParams = {
            Names: [
                rootPath + SlackAppConfigParams.SigningSecret,
                rootPath + SlackAppConfigParams.OAuthToken
            ],
            WithDecryption: true
        };
        // Get slack signing secret from SSM
        ssm.getParameters(ssmParams, function (error, data) {
            if (error) {
                reject("AWSError [" + error.code + "]. " + error.message + " ");
            }
            else {
                var results = data.Parameters;
                if (!results) {
                    reject("Cannot find anything at " + rootPath);
                }
                else {
                    var signingSecret = results.find(function (p) { return p.Name === rootPath + SlackAppConfigParams.SigningSecret; });
                    var OAuthToken = results.find(function (p) { return p.Name === rootPath + SlackAppConfigParams.OAuthToken; });
                    if (!signingSecret) {
                        reject("Cannot find signingSecret " + rootPath);
                    }
                    else if (!OAuthToken) {
                        reject("Cannot find OAuthToken " + rootPath);
                    }
                    else {
                        var config = {
                            SigningSecret: signingSecret.Value,
                            OAuthToken: OAuthToken.Value
                        };
                        resolve(config);
                    }
                }
            }
        });
    });
}
exports.getSlackAppConfig = getSlackAppConfig;
function getParameter(path, WithDecryption) {
    if (WithDecryption === void 0) { WithDecryption = false; }
    return new Promise(function (resolve, reject) {
        var ssmParams = {
            Name: path,
            WithDecryption: WithDecryption
        };
        ssm.getParameter(ssmParams, function (error, data) {
            if (error) {
                reject("AWSError [" + error.code + "]. " + error.message + " ");
            }
            else {
                if (data.Parameter === undefined || data.Parameter.Value === undefined) {
                    resolve(undefined);
                }
                else {
                    resolve(data.Parameter.Value);
                }
            }
        });
    });
}
exports.getParameter = getParameter;
/**
 * Get signing secret from SSM
 * @param token App Vertification Token
 */
function getParameters(paths, WithDecryption) {
    if (WithDecryption === void 0) { WithDecryption = false; }
    return new Promise(function (resolve, reject) {
        var ssmParams = {
            Names: paths,
            WithDecryption: WithDecryption
        };
        ssm.getParameters(ssmParams, function (error, data) {
            if (error) {
                reject("AWSError [" + error.code + "]. " + error.message + " ");
            }
            else {
                var Invalid = data.InvalidParameters ? data.InvalidParameters : [];
                var ParameterList = data.Parameters ? data.Parameters : [];
                var Parameters = ParameterList.reduce(function (obj, param) {
                    obj[param.Name] = param.Value;
                    return obj;
                }, {});
                resolve({ Invalid: Invalid, Parameters: Parameters });
            }
        });
    });
}
exports.getParameters = getParameters;
function putParameter(path, Value, Type, Overwrite) {
    if (Overwrite === void 0) { Overwrite = false; }
    var ssmParams = {
        Name: path,
        Value: Value,
        Type: Type,
        Overwrite: Overwrite
    };
    ssm.putParameter(ssmParams, function (err, data) {
        if (err) {
            console.log(err);
        }
        else {
            console.log(data);
        }
    });
}
exports.putParameter = putParameter;
//# sourceMappingURL=ssm.js.map