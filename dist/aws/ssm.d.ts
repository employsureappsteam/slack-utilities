import { SlackConfig } from "../models/service";
export declare type ParametersMap = {
    [path: string]: string;
};
export interface ParametersResult {
    Parameters: ParametersMap;
    Invalid: string[];
}
/**
 * Get signing secret from SSM
 * @param token App Vertification Token
 */
export declare function getSlackAppConfig(rootPath: string): Promise<SlackConfig>;
export declare function getParameter(path: string, WithDecryption?: boolean): Promise<string | undefined>;
/**
 * Get signing secret from SSM
 * @param token App Vertification Token
 */
export declare function getParameters(paths: string[], WithDecryption?: boolean): Promise<ParametersResult>;
export declare function putParameter(path: string, Value: string, Type: 'String' | 'SecureString', Overwrite?: boolean): void;
