import { Lambda } from "aws-sdk";
/**
 *
 * @param functionName name of
 * @param eventPayload
 */
export declare function invokeLambda<T>(functionName: string, eventPayload: T): Promise<import("aws-sdk/lib/request").PromiseResult<Lambda.InvocationResponse, import("aws-sdk").AWSError>>;
