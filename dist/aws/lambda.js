"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.invokeLambda = void 0;
var aws_sdk_1 = require("aws-sdk");
var employsure_core_1 = require("employsure-core");
/**
 *
 * @param functionName name of
 * @param eventPayload
 */
function invokeLambda(functionName, eventPayload) {
    var lambda = new aws_sdk_1.Lambda({
        region: employsure_core_1.AWS_REGION_DEFAULT,
        apiVersion: employsure_core_1.AWS_LAMBDA_API_VERSION
    });
    var invokeRequest = {
        FunctionName: functionName,
        Payload: JSON.stringify(eventPayload),
        InvocationType: 'Event'
    };
    return lambda.invoke(invokeRequest).promise();
}
exports.invokeLambda = invokeLambda;
//# sourceMappingURL=lambda.js.map