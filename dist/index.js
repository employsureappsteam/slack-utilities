"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __exportStar = (this && this.__exportStar) || function(m, exports) {
    for (var p in m) if (p !== "default" && !Object.prototype.hasOwnProperty.call(exports, p)) __createBinding(exports, m, p);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.getSlackAppConfig = exports.postSlackMessage = exports.updateSlackMessage = exports.AppConfigPaths = exports.ChannelIDs = void 0;
var employsure_core_1 = require("employsure-core");
Object.defineProperty(exports, "ChannelIDs", { enumerable: true, get: function () { return employsure_core_1.ChannelIDs; } });
Object.defineProperty(exports, "AppConfigPaths", { enumerable: true, get: function () { return employsure_core_1.AppConfigPaths; } });
/* Actual functions to send to slack. */
var slack_1 = require("./libs/slack");
Object.defineProperty(exports, "updateSlackMessage", { enumerable: true, get: function () { return slack_1.updateSlackMessage; } });
Object.defineProperty(exports, "postSlackMessage", { enumerable: true, get: function () { return slack_1.postSlackMessage; } });
/* Supporting functions */
var ssm_1 = require("./aws/ssm");
Object.defineProperty(exports, "getSlackAppConfig", { enumerable: true, get: function () { return ssm_1.getSlackAppConfig; } });
/* Supporting models */
__exportStar(require("./models/messages"), exports);
__exportStar(require("./models/attachments"), exports);
__exportStar(require("./models/actions"), exports);
__exportStar(require("./models/slackPayloads"), exports);
//# sourceMappingURL=index.js.map