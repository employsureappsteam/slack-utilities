export declare class SlackEmptyResponse {
    statusCode: number;
    constructor();
}
export declare class SlackTextResponse extends SlackEmptyResponse {
    headers: {
        'content-type': string;
    };
    body: string;
    constructor(text: string);
}
