"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UpdateMessageArguments = exports.PostMessageArguments = exports.CODE_BLOCK = void 0;
exports.CODE_BLOCK = "```";
var PostMessageArguments = /** @class */ (function () {
    function PostMessageArguments(text, channel, token) {
        this.text = text;
        this.channel = channel;
        this.token = token;
        this.attachments = [];
    }
    PostMessageArguments.prototype.attach = function (att) {
        this.attachments = this.attachments.concat(att);
    };
    return PostMessageArguments;
}());
exports.PostMessageArguments = PostMessageArguments;
/**
 * Meets requirements for chat.update argument.
 * https://api.slack.com/methods/chat.update
 */
var UpdateMessageArguments = /** @class */ (function () {
    function UpdateMessageArguments(token, channel, text, ts) {
        this.token = token;
        this.channel = channel;
        this.text = text;
        this.ts = ts;
        // Don't post as user please.
        this.as_user = false;
        // Force removal of attachments. Default behaviour. 
        this.attachments = [];
    }
    UpdateMessageArguments.prototype.attach = function (att) {
        this.attachments = this.attachments.concat(att);
    };
    return UpdateMessageArguments;
}());
exports.UpdateMessageArguments = UpdateMessageArguments;
//# sourceMappingURL=messages.js.map