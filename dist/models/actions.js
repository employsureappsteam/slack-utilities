"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.Selection = exports.DangerButton = exports.PrimaryButton = exports.DefaultButton = exports.ActionNames = void 0;
/**
 * Action names to be
 */
exports.ActionNames = {
    Acknowledge: "acknowledge",
    Retry: "retry",
    Ping: "ping",
    EmployeeSelect: "employee_select",
    Run: "run",
    Cancel: "cancel",
    Confirm: "confirm",
    ProvisionWorkplace: "provisionWorkplace",
    UpdateWorkplaceAdmins: "updateWorkplaceAdmins"
};
var DefaultButton = /** @class */ (function () {
    function DefaultButton(name, text, value) {
        if (value === void 0) { value = ""; }
        this.style = 'default';
        this.type = 'button';
        this.name = name;
        this.text = text;
        this.value = value;
    }
    return DefaultButton;
}());
exports.DefaultButton = DefaultButton;
var PrimaryButton = /** @class */ (function (_super) {
    __extends(PrimaryButton, _super);
    function PrimaryButton(name, text, value) {
        if (value === void 0) { value = ""; }
        var _this = _super.call(this, name, text, value) || this;
        _this.style = 'primary';
        return _this;
    }
    return PrimaryButton;
}(DefaultButton));
exports.PrimaryButton = PrimaryButton;
var DangerButton = /** @class */ (function (_super) {
    __extends(DangerButton, _super);
    function DangerButton(name, text, value) {
        if (value === void 0) { value = ""; }
        var _this = _super.call(this, name, text, value) || this;
        _this.style = 'danger';
        return _this;
    }
    return DangerButton;
}(DefaultButton));
exports.DangerButton = DangerButton;
var Selection = /** @class */ (function () {
    function Selection(name, text, options) {
        this.value = "";
        this.type = 'select';
        this.name = name;
        this.text = text;
        this.options = options;
    }
    return Selection;
}());
exports.Selection = Selection;
//# sourceMappingURL=actions.js.map