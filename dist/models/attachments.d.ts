import { MessageAttachment, AttachmentAction } from "@slack/client";
export declare const CallbackIDs: {
    FailedSync: string;
    ProvisionSelection: string;
    ProvisionConfirmation: string;
    NZLeadScrapeConfirmation: string;
    FWCScrapeConfirmation: string;
    WorkplaceProvisioningAction: string;
    WorkplaceProvisionRetry: string;
};
export declare class DefaultAttachment implements MessageAttachment {
    title: string;
    text: string;
    pretext: string;
    footer: string;
    fallback: string;
    color: 'good' | 'warning' | 'danger' | string;
    actions: AttachmentAction[];
    callback_id: string;
    addAction(action: AttachmentAction): void;
    removeAllActions(): void;
    appendText(text: string): void;
    constructor(title: string, text: string, callback_id?: string, notifyChannel?: boolean);
}
export declare class TopicAttachment extends DefaultAttachment {
    constructor(title: string, message: string, color: string, details: string);
}
/**
 * Class for a failed sync job attachment.
 */
export declare class FailedMessageAttachement extends DefaultAttachment {
    constructor(title: string, callbackId: string, value: string);
}
export declare class ProvisionEmployeeSelection extends DefaultAttachment {
    constructor(text: string, emails: string[]);
}
