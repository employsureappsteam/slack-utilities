import { ActionPayload } from "./slackPayloads";
import { SlashCommandPayload } from "./slackPayloads";
export declare type SlashCommandHandler = (arg: SlashCommandPayload, config: SlackConfig) => Promise<{
    success: boolean;
    message?: string;
}>;
export declare type ActionHandler = (arg: ActionPayload, config: SlackConfig) => Promise<{
    success: boolean;
    message: string;
}>;
export interface SlackConfig {
    readonly SigningSecret: string;
    readonly OAuthToken: string;
}
export interface ISlashCommandService {
    ConfigPath: string;
    Execute: SlashCommandHandler;
    AllowedChannels?: Array<string>;
}
export interface ActionService {
    ConfigPath: string;
    Execute: ActionHandler;
}
