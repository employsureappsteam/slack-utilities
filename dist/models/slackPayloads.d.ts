import { DefaultAttachment } from "./attachments";
import { MessageAttachment } from "@slack/client";
export interface SlashCommandPayload {
    command: string;
    text: string;
    user_name: string;
    user_id: string;
    token: string;
    channel_id: string;
    channel_name: string;
    response_url: string;
    trigger_id: string;
}
/**
 * The payload object received when a menu item is selected.
 *
 * Built from inspecting object received from Slack. I couldn't find docs/spec.
 *
 * MAY CHANGE!
 */
export interface ActionPayload {
    token: string;
    callback_id: string;
    type: string;
    trigger_id: string;
    response_url: string;
    action_ts: string;
    message_ts: string;
    attachment_id: string;
    is_app_unfurl: string;
    team: {
        id: string;
        domain: string;
    };
    channel: {
        id: string;
        name: string;
    };
    user: {
        id: string;
        name: string;
    };
    original_message: {
        text: string;
        bot_id: string;
        type: string;
        subtype: string;
        ts: string;
        attachments: DefaultAttachment[];
    };
    actions: [
        {
            name: string;
            type: string;
            value: any;
            selected_options: [
                {
                    value: string;
                }
            ];
        }
    ];
}
export interface PostMessageResponsePayload {
    "ok": boolean;
    "channel"?: string;
    "ts"?: string;
    "message"?: {
        "text"?: string;
        "username"?: string;
        "bot_id"?: string;
        "type"?: string;
        "subtype"?: string;
        "ts"?: string;
        "attachments"?: MessageAttachment[];
    };
    "scopes"?: string[];
    "acceptedScopes"?: string[];
}
export interface UpdateMessageResponsePayload {
}
