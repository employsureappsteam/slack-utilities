import { MessageAttachment, ChatPostMessageArguments, ChatUpdateArguments } from "@slack/client";
export declare const CODE_BLOCK = "```";
export declare class PostMessageArguments implements ChatPostMessageArguments {
    attachments: MessageAttachment[];
    channel: string;
    token: string;
    text: string;
    constructor(text: string, channel: string, token: string);
    attach(att: MessageAttachment | MessageAttachment[]): void;
}
/**
 * Meets requirements for chat.update argument.
 * https://api.slack.com/methods/chat.update
 */
export declare class UpdateMessageArguments implements ChatUpdateArguments {
    token: string;
    channel: string;
    text: string;
    ts: string;
    as_user?: boolean;
    attachments: MessageAttachment[];
    link_names?: boolean;
    parse?: 'full' | 'none';
    constructor(token: string, channel: string, text: string, ts: string);
    attach(att: MessageAttachment | MessageAttachment[]): void;
}
