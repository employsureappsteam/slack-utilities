"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.ProvisionEmployeeSelection = exports.FailedMessageAttachement = exports.TopicAttachment = exports.DefaultAttachment = exports.CallbackIDs = void 0;
var actions_1 = require("./actions");
var slack_1 = require("../constants/slack");
var slack_2 = require("../libs/slack");
var messages_1 = require("./messages");
exports.CallbackIDs = {
    FailedSync: "PureCloudSyncFail",
    ProvisionSelection: "ProvisionEmployeeSelection",
    ProvisionConfirmation: "ProvisionEmployeeConfirmation",
    NZLeadScrapeConfirmation: "NZLeadScrapeConfirmation",
    FWCScrapeConfirmation: "FWCScrapeConfirmation",
    WorkplaceProvisioningAction: "ProvisionWorkplace",
    WorkplaceProvisionRetry: "WorkplaceProvisionRetry"
};
var DefaultAttachment = /** @class */ (function () {
    function DefaultAttachment(title, text, callback_id, notifyChannel) {
        if (callback_id === void 0) { callback_id = ""; }
        if (notifyChannel === void 0) { notifyChannel = false; }
        this.title = "";
        this.text = "";
        this.pretext = "";
        this.footer = "";
        /* Message displayed to users using an interface that does not support attachments or interactive messages */
        this.fallback = "";
        /* Array of attachments to be set in the constructor. */
        this.actions = [];
        this.title = title;
        this.text = text;
        this.color = slack_2.lookupMessageTypeColor(slack_1.SlackMessageTypes.Info);
        this.callback_id = callback_id;
        // Append channel notification to message title if requested
        if (notifyChannel) {
            this.text += " <!channel>";
        }
    }
    DefaultAttachment.prototype.addAction = function (action) {
        this.actions.push(action);
    };
    DefaultAttachment.prototype.removeAllActions = function () {
        this.actions = [];
    };
    DefaultAttachment.prototype.appendText = function (text) {
        this.text = this.text + "\n" + text;
    };
    return DefaultAttachment;
}());
exports.DefaultAttachment = DefaultAttachment;
var TopicAttachment = /** @class */ (function (_super) {
    __extends(TopicAttachment, _super);
    function TopicAttachment(title, message, color, details) {
        var _this = _super.call(this, title, "") || this;
        var detailsWrapped = details ? (messages_1.CODE_BLOCK + "\n" + details + "\n" + messages_1.CODE_BLOCK) : '';
        _this.text = message + "\n" + detailsWrapped;
        _this.color = color;
        return _this;
    }
    return TopicAttachment;
}(DefaultAttachment));
exports.TopicAttachment = TopicAttachment;
/**
 * Class for a failed sync job attachment.
 */
var FailedMessageAttachement = /** @class */ (function (_super) {
    __extends(FailedMessageAttachement, _super);
    function FailedMessageAttachement(title, callbackId, value) {
        var _this = _super.call(this, title, "", callbackId, true) || this;
        _this.color = slack_2.lookupMessageTypeColor(slack_1.SlackMessageTypes.Failed);
        _this.callback_id = callbackId;
        _this.addAction(new actions_1.DefaultButton("acknowledge", "Acknowledge", "acknowledge"));
        _this.addAction(new actions_1.DefaultButton("retry", "Retry Sync", value));
        return _this;
    }
    return FailedMessageAttachement;
}(DefaultAttachment));
exports.FailedMessageAttachement = FailedMessageAttachement;
var ProvisionEmployeeSelection = /** @class */ (function (_super) {
    __extends(ProvisionEmployeeSelection, _super);
    function ProvisionEmployeeSelection(text, emails) {
        var _this = _super.call(this, "Provision Request", text, exports.CallbackIDs.ProvisionSelection, false) || this;
        // Prepare emails into valid selection options for slack
        var options = [];
        emails.map(function (e) {
            options.push({
                text: e,
                value: e
            });
        });
        _this.addAction(new actions_1.Selection(actions_1.ActionNames.EmployeeSelect, "Employees", options));
        return _this;
    }
    return ProvisionEmployeeSelection;
}(DefaultAttachment));
exports.ProvisionEmployeeSelection = ProvisionEmployeeSelection;
//# sourceMappingURL=attachments.js.map