import { AttachmentAction, OptionField } from "@slack/client";
/**
 * Action names to be
 */
export declare const ActionNames: {
    Acknowledge: string;
    Retry: string;
    Ping: string;
    EmployeeSelect: string;
    Run: string;
    Cancel: string;
    Confirm: string;
    ProvisionWorkplace: string;
    UpdateWorkplaceAdmins: string;
};
export declare class DefaultButton implements AttachmentAction {
    text: string;
    name: string;
    value: string;
    style: 'default' | 'primary' | 'danger';
    type: 'select' | 'button';
    constructor(name: string, text: string, value?: string);
}
export declare class PrimaryButton extends DefaultButton {
    constructor(name: string, text: string, value?: string);
}
export declare class DangerButton extends DefaultButton {
    constructor(name: string, text: string, value?: string);
}
export declare class Selection implements AttachmentAction {
    name: string;
    text: string;
    value: string;
    type: 'select' | 'button';
    options: OptionField[];
    constructor(name: string, text: string, options: OptionField[]);
}
