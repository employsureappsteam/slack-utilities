export interface SnsMessage {
    Errors?: Array<{
        title: string;
        message: string;
        details: string;
    }>;
    Logs?: Array<{
        title: string;
        message: string;
        details: string;
    }>;
    [key: string]: any;
}
