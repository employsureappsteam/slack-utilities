"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.SlackTextResponse = exports.SlackEmptyResponse = void 0;
var SlackEmptyResponse = /** @class */ (function () {
    function SlackEmptyResponse() {
        this.statusCode = 200;
    }
    return SlackEmptyResponse;
}());
exports.SlackEmptyResponse = SlackEmptyResponse;
var SlackTextResponse = /** @class */ (function (_super) {
    __extends(SlackTextResponse, _super);
    function SlackTextResponse(text) {
        var _this = _super.call(this) || this;
        _this.headers = {
            'content-type': 'application/json'
        };
        _this.body = JSON.stringify({
            text: text
        });
        return _this;
    }
    return SlackTextResponse;
}(SlackEmptyResponse));
exports.SlackTextResponse = SlackTextResponse;
//# sourceMappingURL=callbackResults.js.map