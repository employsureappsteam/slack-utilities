"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.postMessageToDevelopment = exports.updateSlackMessage = exports.postEphemeralMessage = exports.postSlackMessage = exports.VerifySlackSignature = exports.parseSlashPayload = exports.lookupMessageTypeColor = void 0;
var crypto_1 = require("crypto");
var aws_sdk_1 = require("aws-sdk");
// import { LambdaNames, AWSConfig } from "../constants/aws";
var slack_1 = require("../constants/slack");
var messages_1 = require("../models/messages");
var ssm_1 = require("../aws/ssm");
var client_1 = require("@slack/client");
var employsure_core_1 = require("employsure-core");
/**
 * Get colour for appropriate message type
 *
 * @param attachment MessageAttachment Object to be modified
 * @param color 6 Digit hex code of color
 */
function lookupMessageTypeColor(type) {
    switch (type) {
        case slack_1.SlackMessageTypes.Success: return "#4dcc0d";
        case slack_1.SlackMessageTypes.Warning: return "#c2f013";
        case slack_1.SlackMessageTypes.Failed: return "#d60a0a";
        case slack_1.SlackMessageTypes.Info: return "#2B387F";
        default: return "#000000";
    }
}
exports.lookupMessageTypeColor = lookupMessageTypeColor;
/**
 * Parse URI from slack message into object
 *
 * @param uri URI in the format "fieldA=textA&fieldB=textB"
 */
function parseSlashPayload(uri) {
    return decodeURIComponent(uri)
        .split("&")
        .reduce(function (accum, val) {
        var kvPair = val.split("=");
        accum[kvPair[0]] = kvPair[1];
        return accum;
    }, {});
}
exports.parseSlashPayload = parseSlashPayload;
/**
 * Verify the signature of a request received from slack
 *
 * @param request
 * @param path path of secret in SSM
 */
function VerifySlackSignature(request, secret) {
    // Time stamp passed by slack does not have milliseconds.
    var timeStamp = request.headers["X-Slack-Request-Timestamp"];
    var slackSignature = request.headers["X-Slack-Signature"];
    var sigBaseString = "v0:" + timeStamp + ":" + request.body;
    var hmac = crypto_1.createHmac("sha256", secret)
        .update(sigBaseString);
    var computedSignature = "v0=" + hmac.digest("hex");
    return computedSignature === slackSignature;
}
exports.VerifySlackSignature = VerifySlackSignature;
/**
 * Invoke lambda function to post slack message.
 *
 * @param slackMessage
 */
function postSlackMessage(slackMessage) {
    // console.log(`Preparing to invoke postSlackMessage: ${JSON.stringify(slackMessage)}`);
    var params = {
        FunctionName: employsure_core_1.LambdaNamesMap.postSlackMessage,
        Payload: JSON.stringify(slackMessage)
    };
    var lambda = new aws_sdk_1.Lambda({
        region: employsure_core_1.AWS_REGION_DEFAULT,
        apiVersion: employsure_core_1.AWS_LAMBDA_API_VERSION
    });
    return lambda.invoke(params).promise()
        .then(function (response) {
        if (response.StatusCode === 200) {
            if (typeof (response.Payload) === "string") {
                return JSON.parse(response.Payload);
            }
            else {
                return Promise.reject("Response Payload is not a string " + response);
            }
        }
        else {
            return Promise.reject("Response statuscode is not 200.");
        }
    })
        .catch(function (error) {
        console.log(error);
        return error;
    });
}
exports.postSlackMessage = postSlackMessage;
function postEphemeralMessage(user_id, slackMessage) {
    return __awaiter(this, void 0, void 0, function () {
        var e_message;
        return __generator(this, function (_a) {
            e_message = {
                channel: slackMessage.channel,
                text: slackMessage.text,
                user: user_id,
                attachments: slackMessage.attachments,
                link_names: true,
                parse: 'none'
            };
            return [2 /*return*/, new client_1.WebClient(slackMessage.token).chat.postEphemeral(e_message)
                    .then(function (data) {
                    console.log("Message sent: " + JSON.stringify(data, null, 4));
                    return data;
                })
                    .catch(function (error) {
                    console.error(error);
                    return error;
                })];
        });
    });
}
exports.postEphemeralMessage = postEphemeralMessage;
/**
 * Update Message
 *
 * @param arg Arguments for updating a particular message.
 */
function updateSlackMessage(arg) {
    // console.log(`Preparing to invoke updateSlackMessage: ${JSON.stringify(arg)}`);
    var params = {
        FunctionName: employsure_core_1.LambdaNamesMap.updateSlackMessage,
        Payload: JSON.stringify(arg)
    };
    var lambda = new aws_sdk_1.Lambda({
        region: employsure_core_1.AWS_REGION_DEFAULT,
        apiVersion: employsure_core_1.AWS_LAMBDA_API_VERSION
    });
    return lambda.invoke(params).promise()
        .then(function (response) {
        if (response.StatusCode === 200) {
            if (typeof (response.Payload) === "string") {
                return JSON.parse(response.Payload);
            }
            else {
                return Promise.reject("Response Payload is not a string " + response);
            }
        }
        else {
            return Promise.reject("Response statuscode is not 200.");
        }
    })
        .catch(function (error) {
        console.log(error);
        return error;
    });
}
exports.updateSlackMessage = updateSlackMessage;
function postMessageToDevelopment(attachment, type) {
    if (type === void 0) { type = 'log'; }
    return __awaiter(this, void 0, void 0, function () {
        var config, message;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, ssm_1.getSlackAppConfig(employsure_core_1.AppConfigPaths.Development)];
                case 1:
                    config = _a.sent();
                    message = new messages_1.PostMessageArguments("", employsure_core_1.ChannelIDs.EngAlertsDev, config.OAuthToken);
                    if (type === 'error') {
                        attachment.color = 'danger';
                    }
                    message.attach(attachment);
                    return [2 /*return*/, postSlackMessage(message)];
            }
        });
    });
}
exports.postMessageToDevelopment = postMessageToDevelopment;
//# sourceMappingURL=slack.js.map