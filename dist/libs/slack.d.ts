import { PostMessageArguments, UpdateMessageArguments } from "../models/messages";
import { PostMessageResponsePayload, UpdateMessageResponsePayload } from "../models/slackPayloads";
import { DefaultAttachment } from "../models/attachments";
/**
 * Get colour for appropriate message type
 *
 * @param attachment MessageAttachment Object to be modified
 * @param color 6 Digit hex code of color
 */
export declare function lookupMessageTypeColor(type: number): string;
/**
 * Parse URI from slack message into object
 *
 * @param uri URI in the format "fieldA=textA&fieldB=textB"
 */
export declare function parseSlashPayload(uri: string): any;
/**
 * Verify the signature of a request received from slack
 *
 * @param request
 * @param path path of secret in SSM
 */
export declare function VerifySlackSignature(request: any, secret: string): boolean;
/**
 * Invoke lambda function to post slack message.
 *
 * @param slackMessage
 */
export declare function postSlackMessage(slackMessage: PostMessageArguments): Promise<PostMessageResponsePayload>;
export declare function postEphemeralMessage(user_id: string, slackMessage: PostMessageArguments): Promise<any>;
/**
 * Update Message
 *
 * @param arg Arguments for updating a particular message.
 */
export declare function updateSlackMessage(arg: UpdateMessageArguments): Promise<UpdateMessageResponsePayload>;
export declare function postMessageToDevelopment(attachment: DefaultAttachment, type?: 'error' | 'log'): Promise<PostMessageResponsePayload>;
